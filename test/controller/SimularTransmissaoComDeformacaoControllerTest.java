/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import model.Material;
import model.RegistoDados;
import model.SimuladorComDeformacao;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class SimularTransmissaoComDeformacaoControllerTest {

    private RegistoDados registoDados;
    private Material material;
    private HashMap<String, Float> listaMaterial;

    public SimularTransmissaoComDeformacaoControllerTest() {
        this.registoDados = RegistoDados.getInstance();
        this.registoDados.preencherListas();
    }

    /**
     * Test of getRegistoDados method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetRegistoDados() {
        System.out.println("getRegistoDados");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        RegistoDados expResult = this.registoDados;
        RegistoDados result = instance.getRegistoDados();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaMateriaisNucleo method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetListaMateriaisNucleo() {
        System.out.println("getListaMateriaisNucleo");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        this.registoDados = instance.getRegistoDados();
        HashMap<String, Float> listaMaterial = new HashMap<>();
        listaMaterial.put("Vidro-baixo indice", 1.5f);
        listaMaterial.put("Vidro-alto indice", 1.9f);
        listaMaterial.put("Silex vidro", 1.62f);
        listaMaterial.put("Vidro ótico", 1.54f);
        listaMaterial.put("Glicerina", 1.47f);
        listaMaterial.put("Policarbonato", 1.59f);
        listaMaterial.put("Silica", 1.458f);
        listaMaterial.put("Acrilico", 1.49f);
        Set<String> expResult = listaMaterial.keySet();
        Set<String> result = instance.getListaMateriaisNucleo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMaterialNucleo method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetMaterialNucleo() {
        System.out.println("getMaterialNucleo");
        String materialNucleo = "Silica";
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        instance.getMaterialNucleo(materialNucleo);
    }

    /**
     * Test of getListaMateriaisBainha method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetListaMateriaisBainha() {
        System.out.println("getListaMateriaisBainha");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> lista = new ArrayList<>();
        lista.add("Acrilico");
        lista.add("Glicerina");
        int expResult = lista.size();
        int result = instance.getListaMateriaisBainha().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMaterialBainha method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetMaterialBainha() {
        System.out.println("getMaterialBainha");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
    }

    /**
     * Test of createFO method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCreateFO() {
        System.out.println("createFO");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
    }

    /**
     * Test of getListaComprimentoOnda method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetListaComprimentoOnda() {
        System.out.println("getListaComprimentoOnda");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        int expResult = this.registoDados.getListaComprimentoOnda().size();
        int result = instance.getListaComprimentoOnda().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of getComprimentoOnda method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetComprimentoOnda() {
        System.out.println("getComprimentoOnda");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
    }

    /**
     * Test of getListaDiametroNucleo method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetListaDiametroNucleo() {
        System.out.println("getListaDiametroNucleo");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        int expResult = this.registoDados.getListaDiametroNucleo().size();
        int result = instance.getListaDiametroNucleo().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDiametroNucleo method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testGetDiametroNucleo() {
        System.out.println("getDiametroNucleo");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
    }

    /**
     * Test of createSimulador method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCreateSimulador() {
        System.out.println("createSimulador");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
    }

    /**
     * Test of adicionarDadosSimulacao method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testAdicionarDadosSimulacao() {
        System.out.println("adicionarDadosSimulacao");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada = -1.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        boolean expResult = false;
        boolean result = instance.adicionarDadosSimulacao(potenciaEntrada, anguloIncidencia, comprimentoFibra, potenciaEntrada, potenciaEntrada);
        assertEquals(expResult, result);
    }

    /**
     * Test of calcularAnguloMaximoAceitacao method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCalcularAnguloMaximoAceitacao() {
        System.out.println("calcularAnguloMaximoAceitacao");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada12 = 9.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        float distanciaSemDeformacao = 2;
        float variacaoDiametroNucleo = 0.000000005f;
        instance.adicionarDadosSimulacao(potenciaEntrada12,
                            anguloIncidencia, comprimentoFibra,
                            distanciaSemDeformacao, variacaoDiametroNucleo);
        double expResult = 0.303116;
        double result = instance.calcularAnguloMaximoAceitacao();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularConeAceitacao method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCalcularConeAceitacao() {
        System.out.println("calcularConeAceitacao");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada12 = 9.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        float distanciaSemDeformacao = 2;
        float variacaoDiametroNucleo = 0.000000005f;
        instance.adicionarDadosSimulacao(potenciaEntrada12, anguloIncidencia, comprimentoFibra, distanciaSemDeformacao, variacaoDiametroNucleo);
        instance.calcularAnguloMaximoAceitacao();
        double expResult = 0.60623;
        double result = instance.calcularConeAceitacao();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularAberturaNumerica method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCalcularAberturaNumerica() {
        System.out.println("calcularAberturaNumerica");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada12 = 9.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        float distanciaSemDeformacao = 2;
        float variacaoDiametroNucleo = 0.000000005f;
        instance.adicionarDadosSimulacao(potenciaEntrada12,
                            anguloIncidencia, comprimentoFibra,
                            distanciaSemDeformacao, variacaoDiametroNucleo);
        instance.calcularAnguloMaximoAceitacao();
        instance.calcularConeAceitacao();
        double expResult = 0.298496f;
        double result = instance.calcularAberturaNumerica();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularAnguloCritico method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCalcularAnguloCritico() {
        System.out.println("calcularAnguloCritico");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada12 = 9.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        float distanciaSemDeformacao = 2;
        float variacaoDiametroNucleo = 0.000000005f;
        instance.adicionarDadosSimulacao(potenciaEntrada12,
                            anguloIncidencia, comprimentoFibra,
                            distanciaSemDeformacao, variacaoDiametroNucleo);
        instance.calcularAnguloMaximoAceitacao();
        instance.calcularConeAceitacao();
        instance.calcularAberturaNumerica();
        double expResult = 1.37046f;
        double result = instance.calcularAnguloCritico();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularPotenciaSaida method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCalcularPotenciaSaida() {
        System.out.println("calcularPotenciaSaida");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada12 = 9.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        float distanciaSemDeformacao = 2;
        float variacaoDiametroNucleo = 0.000000005f;
        instance.adicionarDadosSimulacao(potenciaEntrada12,
                            anguloIncidencia, comprimentoFibra,
                            distanciaSemDeformacao, variacaoDiametroNucleo);
        instance.calcularAnguloMaximoAceitacao();
        instance.calcularConeAceitacao();
        instance.calcularAberturaNumerica();
        instance.calcularAnguloCritico();
        double expResult = 7.554830;
        double result = instance.calcularPotenciaSaida();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularFrequenciaNormalizada method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testCalcularFrequenciaNormalizada() {
        System.out.println("calcularFrequenciaNormalizada");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada12 = 9.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        float distanciaSemDeformacao = 2;
        float variacaoDiametroNucleo = 0.000000005f;
        instance.adicionarDadosSimulacao(potenciaEntrada12,
                            anguloIncidencia, comprimentoFibra,
                            distanciaSemDeformacao, variacaoDiametroNucleo);
        instance.calcularAnguloMaximoAceitacao();
        instance.calcularConeAceitacao();
        instance.calcularAberturaNumerica();
        instance.calcularAnguloCritico();
        double expResult = 0.18906f;
        double result = instance.calcularFrequenciaNormalizada();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of classificaFibra method, of class
     * SimularTransmissaoComDeformacaoController.
     */
    @Test
    public void testClassificaFibra() {
        System.out.println("classificaFibra");
        SimularTransmissaoComDeformacaoController instance = new SimularTransmissaoComDeformacaoController();
        instance.getRegistoDados();
        instance.getListaMateriaisNucleo();
        String materialNucleo = "Vidro-baixo indice";
        instance.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = new ArrayList<>();
        listaMateriaisBainha.add("Acrilico");
        listaMateriaisBainha.add("Glicerina");
        instance.getListaMateriaisBainha();
        String materialBainha = "Glicerina";
        instance.getMaterialBainha(materialBainha);
        instance.createFO();
        instance.getListaComprimentoOnda();
        String comprimentoOnda = "Visivel-Laranja";
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getListaDiametroNucleo();
        String diametroNucleo = "5";
        instance.getDiametroNucleo(diametroNucleo);
        instance.createSimulador();
        float potenciaEntrada12 = 9.0f;
        float anguloIncidencia = 10.0f;
        float comprimentoFibra = 4.0f;
        float distanciaSemDeformacao = 2;
        float variacaoDiametroNucleo = 0.000000005f;
        instance.adicionarDadosSimulacao(potenciaEntrada12, 
                            anguloIncidencia, comprimentoFibra, 
                            distanciaSemDeformacao, variacaoDiametroNucleo);
        instance.calcularAnguloMaximoAceitacao();
        instance.calcularConeAceitacao();
        instance.calcularAberturaNumerica();
        instance.calcularAnguloCritico();
        double valor = instance.calcularFrequenciaNormalizada();
        String expResult = "Monomodo";
        String result = instance.classificaFibra();
        assertEquals(expResult, result);
    }

}
