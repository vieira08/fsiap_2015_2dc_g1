/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class RegistarNovoMaterialControllerTest {

    public RegistarNovoMaterialControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of adicionarMaterial method, of class
     * RegistarNovoMaterialController.
     */
    @Test
    public void testAdicionarMaterial() {
        System.out.println("adicionarMaterial");
        String nomeMaterial = "madeira";
        float indiceRefracao = 1.5F;
        RegistarNovoMaterialController instance = new RegistarNovoMaterialController();
        boolean expResult = true;
        boolean result = instance.adicionarMaterial(nomeMaterial, indiceRefracao);
        assertEquals(expResult, result);
    }

    /**
     * Test of adicionarMaterial method, of class
     * RegistarNovoMaterialController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAdicionarMaterialNomeVazio() {
        System.out.println("adicionarMaterial");
        String nomeMaterial = "";
        float indiceRefracao = 1.5F;
        RegistarNovoMaterialController instance = new RegistarNovoMaterialController();
        boolean expResult = false;
        boolean result = instance.adicionarMaterial(nomeMaterial, indiceRefracao);
        assertEquals(expResult, result);
    }

    /**
     * Test of adicionarMaterial method, of class
     * RegistarNovoMaterialController.
     */
    public void testAdicionarMaterialNomeJaExistente() {
        System.out.println("adicionarMaterial");
        String nomeMaterial = "Glicerina";
        float indiceRefracao = 1.5F;
        RegistarNovoMaterialController instance = new RegistarNovoMaterialController();
        boolean expResult = false;
        boolean result = instance.adicionarMaterial(nomeMaterial, indiceRefracao);
        assertEquals(expResult, result);
    }

    /**
     * Test of adicionarMaterial method, of class
     * RegistarNovoMaterialController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAdicionarMaterialIndiceInvalido() {
        System.out.println("adicionarMaterial");
        String nomeMaterial = "madeira1";
        float indiceRefracao = 0.5F;
        RegistarNovoMaterialController instance = new RegistarNovoMaterialController();
        boolean expResult = false;
        boolean result = instance.adicionarMaterial(nomeMaterial, indiceRefracao);
    }
}
