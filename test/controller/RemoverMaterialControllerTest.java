/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Set;
import model.RegistoDados;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class RemoverMaterialControllerTest {

    public RemoverMaterialControllerTest() {
    }

    /**
     * Test of listaMateriais method, of class RemoverMaterialController.
     */
    @Test
    public void testListaMateriais() {
        System.out.println("listaMateriais");
        RemoverMaterialController instance = new RemoverMaterialController();
        RegistoDados.getInstance().preencherListas();
        instance.listaMateriais();
        int expResult = 8;
        int result = instance.listaMateriais().size();
        assertEquals(expResult, result);

    }

    /**
     * Test of removerMaterial method, of class RemoverMaterialController.
     */
    @Test
    public void testRemoverMaterial() {
        System.out.println("removerMaterial");
        String material = "Acrilico";
        RemoverMaterialController instance = new RemoverMaterialController();
        RegistoDados.getInstance().preencherListas();
        boolean expResult = true;
        boolean result = instance.removerMaterial(material);
        assertEquals(expResult, result);

    }

}
