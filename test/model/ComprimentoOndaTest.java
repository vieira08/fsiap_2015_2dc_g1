/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class ComprimentoOndaTest {

    private HashMap<String, Float> cmpOnda = new HashMap<String, Float>();

    public ComprimentoOndaTest() {
    }

    /**
     * Test of getListaComprimentoOnda method, of class ComprimentoOnda.
     */
    @Test
    public void testGetListaComprimentoOnda() {
        System.out.println("getListaComprimentoOnda");

        ComprimentoOnda instance = new ComprimentoOnda();
        assertTrue("O resultado deveria ser 0", (instance.getListaComprimentoOnda().size()) == 0);

        instance.preencheListaComprimentoOnda();
        assertTrue("O resultado deveria ser 6", (instance.getListaComprimentoOnda().size()) == 6);
    }

    /**
     * Test of getComprimentoOnda method, of class ComprimentoOnda.
     */
    @Test
    public void testGetComprimentoOnda() {
        System.out.println("getComprimentoOnda");
        ComprimentoOnda instance = new ComprimentoOnda();

        instance.preencheListaComprimentoOnda();
        assertTrue("O resultado deveria ser 0.660", (instance.getComprimentoOnda("Visivel-Vermelho")) == 0.660f);

        assertTrue("O resultado deveria ser 0.580", (instance.getComprimentoOnda("Visivel-Amarelo")) == 0.580f);

    }

    /**
     * Test of preencheListaComprimentoOnda method, of class ComprimentoOnda.
     */
    @Test
    public void testPreencheListaComprimentoOnda() {
        System.out.println("preencheListaComprimentoOnda");
        ComprimentoOnda instance = new ComprimentoOnda();
        instance.preencheListaComprimentoOnda();
        assertTrue("O resultado deveria ser 6", (instance.getListaComprimentoOnda().size()) == 6);
    }

}
