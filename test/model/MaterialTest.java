/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author G01
 */
public class MaterialTest {

    private HashMap<String, Float> listaMaterial;

    public MaterialTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getListaMateriaisNucleo method, of class Material.
     */
    @Test
    public void testGetListaMateriaisNucleo() {
        System.out.println("getListaMateriaisNucleo");
        Material instance = new Material();

        assertTrue("O resultado deveria ser 0", (instance.getListaMateriaisNucleo().size()) == 0);

        instance.preencheListaMateriais();
        assertTrue("O resultado deveria ser 8", (instance.getListaMateriaisNucleo().size()) == 8);

    }

    /**
     * Test of getMaterial method, of class Material.
     */
    @Test
    public void testGetMaterial() {
        System.out.println("getMaterial");

        Material instance = new Material();
        assertTrue("O resultado deveria ser null", (instance.getMaterial(0)) == null);

        instance.preencheListaMateriais();
        assertTrue("O resultado deveria ser Acrilico", (instance.getMaterial(1.49f)).equalsIgnoreCase("Acrilico"));

        assertTrue("O resultado deveria ser Vidro-baixo indice", (instance.getMaterial(1.5f).equalsIgnoreCase("Vidro-baixo indice")));

        assertTrue("O resultado deveria ser Glicerina", (instance.getMaterial(1.47f)).equalsIgnoreCase("Glicerina"));
    }

    /**
     * Test of getListaMateriaisBainha method, of class Material.
     */
    @Test
    public void testGetListaMateriaisBainha() {
        System.out.println("getListaMateriaisBainha");
        Material instance = new Material();
        assertTrue("O resultado deveria ser 0", (instance.getListaMateriaisBainha(2.42000f).size()) == 0);

        instance.preencheListaMateriais();
        assertTrue("O resultado deveria ser 2", (instance.getListaMateriaisBainha(1.5f).size()) == 2);
    }

    /**
     * Test of getIndice method, of class Material.
     */
    @Test
    public void testGetIndice() {
        System.out.println("getIndice");

        Material instance = new Material();
        instance.preencheListaMateriais();
        assertTrue("O resultado deveria ser 1.49f", (instance.getIndice("Acrilico") == 1.49f));

    }


    /**
     * Test of preencheListaMateriais method, of class Material.
     */
    @Test
    public void testPreencheListaMateriais() {
        System.out.println("preencheListaMateriais");
        Material instance = new Material();
        assertTrue("Resultado deveria ser verdadeiro", instance.getListaMateriaisNucleo().size() == 0);

        instance.preencheListaMateriais();
        assertTrue("Resultado deveria ser 8", instance.getListaMateriaisNucleo().size() == 8);
    }

    /**
     * Test of adicionaMaterial method, of class Material.
     */
    @Test
    public void testAdicionaMaterial() {
        System.out.println("adicionaMaterial");
        String nomeMaterial = "madeira";
        float indiceRefracaoMaterial = 1.5F;
        Material instance = new Material();
        boolean expResult = true;
        boolean result = instance.adicionaMaterial(nomeMaterial, indiceRefracaoMaterial);
        assertEquals(expResult, result);
       
    }
    
    /**
     * Test of adicionaMaterial method, of class Material.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAdicionaMaterialNomeInvalido() {
        System.out.println("adicionaMaterial");
        String nomeMaterial = "";
        float indiceRefracaoMaterial = 1.5F;
        Material instance = new Material();
        boolean result = instance.adicionaMaterial(nomeMaterial, indiceRefracaoMaterial);     
    }
    
    /**
     * Test of adicionaMaterial method, of class Material.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAdicionaMaterialIndiceInvalido() {
        System.out.println("adicionaMaterial");
        String nomeMaterial = "madeira";
        float indiceRefracaoMaterial = 0.0F;
        Material instance = new Material();
        boolean result = instance.adicionaMaterial(nomeMaterial, indiceRefracaoMaterial);
       
    }
      /**
     * Test of removerMaterial method, of class Material.
     */
    @Test
    public void testRemoverMaterial() {
        System.out.println("removerMaterial");

        Material instance = new Material();
        instance.preencheListaMateriais();
        assertTrue("O resultado deveria ser Acrilico", instance.removerMaterial("Acrilico"));
        assertTrue("O resultado deveria ser falso", instance.removerMaterial("Acrilico")==false);
        
        assertTrue("O resultado deveria ser Glicerina", instance.removerMaterial("Glicerina"));
    }
}
