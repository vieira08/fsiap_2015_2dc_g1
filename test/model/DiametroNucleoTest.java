/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class DiametroNucleoTest {

    private HashMap<String, Float> listaDiametroNucleo = new HashMap<String, Float>();

    public DiametroNucleoTest() {
    }

    /**
     * Test of getListaDiametroNucleo method, of class DiametroNucleo.
     */
    @Test
    public void testGetListaDiametroNucleo() {
        System.out.println("getListaDiametroNucleo");
        DiametroNucleo instance = new DiametroNucleo();
        assertTrue("O resultado deveria ser 0", (instance.getListaDiametroNucleo().size()) == 0);

        instance.preencheListaDiametroNucleo();
        assertTrue("O resultado deveria ser 6", (instance.getListaDiametroNucleo().size()) == 6);
    }

    /**
     * Test of getDiametroNucleo method, of class DiametroNucleo.
     */
    @Test
    public void testGetDiametroNucleo() {
        System.out.println("getDiametroNucleo");
        String diametro = "";
        DiametroNucleo instance = new DiametroNucleo();

        instance.preencheListaDiametroNucleo();
        assertTrue("O resultado deveria ser 0.000008", (instance.getDiametroNucleo("8")) == 0.000008f);

        assertTrue("O resultado deveria ser 0.000005", (instance.getDiametroNucleo("5")) == 0.000005f);

        assertTrue("O resultado deveria ser  0.000007 ", (instance.getDiametroNucleo("7")) == 0.000007f);

    }

    /**
     * Test of preencheListaDiametroNucleo method, of class DiametroNucleo.
     */
    @Test
    public void testPreencheListaDiametroNucleo() {
        System.out.println("preencheListaDiametroNucleo");
        DiametroNucleo instance = new DiametroNucleo();
        instance.preencheListaDiametroNucleo();

        assertTrue("O resultado deveria ser 6", (instance.getListaDiametroNucleo().size()) == 6);
    }

}
