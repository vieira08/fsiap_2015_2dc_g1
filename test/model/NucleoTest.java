/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class NucleoTest {
    
    public NucleoTest() {
    }

    /**
     * Test of getNomeMaterial method and setNomeMaterial method, of class Nucleo.
     */
    @Test
    public void testGetSetNomeMaterial() {
        System.out.println("getSetNomeMaterial");
        Nucleo instance = new Nucleo();
        String expResult = "nome";
        instance.setNomeMaterial(expResult);
        String result = instance.getNomeMaterial();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIndiceMaterial method and setIndiceMaterial method, of class Nucleo.
     */
    @Test
    public void testGetSetIndiceMaterial() {
        System.out.println("getSetIndiceMaterial");
        Nucleo instance = new Nucleo();
        float expResult = 1.4F;
        instance.setIndiceMaterial(expResult);
        float result = instance.getIndiceMaterial();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class Nucleo.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Nucleo instance = new Nucleo();
        instance.setNomeMaterial("nome");
        instance.setIndiceMaterial(1.4F);
        String expResult = "Nome do núcleo: nome\n Índice do núcleo: " + 1.4F;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Nucleo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Nucleo obj = new Nucleo();
        Nucleo instance = new Nucleo();
        float indice = 1.4F;
        String nome = "nome";
        obj.setNomeMaterial(nome);
        obj.setIndiceMaterial(indice);
        instance.setIndiceMaterial(indice);
        instance.setNomeMaterial(nome);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Nucleo.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals");
        Nucleo obj = new Nucleo();
        Nucleo instance = new Nucleo();
        float indice = 1.4F;
        String nome = "nome";
        String nome2 = "nome2";
        obj.setNomeMaterial(nome);
        obj.setIndiceMaterial(indice);
        instance.setIndiceMaterial(indice);
        instance.setNomeMaterial(nome2);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
