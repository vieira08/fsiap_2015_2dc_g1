/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class SimuladorTest {

    FibraOtica fo = new FibraOtica();
    ComprimentoOnda compOnda = new ComprimentoOnda();
    DiametroNucleo diametroNucleo = new DiametroNucleo();

    public SimuladorTest() {
    }

    /**
     * Test of setAndGgetPotenciaEntrada method, of class Simulador.
     */
    @Test
    public void testSetAndGetPotenciaEntrada() {
        System.out.println("setAndGetPotenciaEntrada");

        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();

        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));

        instance.setPotenciaEntrada(600f);
        assertTrue("O resultado deveria ser 600", instance.getPotenciaEntrada() == 600);

        instance.setPotenciaEntrada(700f);
        assertTrue("O resultado deveria ser 700", instance.getPotenciaEntrada() == 700);
    }

    /**
     * Test of setAndGetFo method, of class Simulador.
     */
    @Test
    public void testSetAndGetFo() {
        System.out.println("setAndGetFo");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();

        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Diamante", 2.14f);

        fo.setNucleo(nucleo);
        fo.setBainha(bainha);

        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));

        FibraOtica fo2 = new FibraOtica(nucleo, bainha);
        instance.setFo(fo2);
        assertEquals("Deveria ser verdadeiro", instance.getFo(), fo2);
    }

    /**
     * Test of setAndGetComprimentoFibra method, of class Simulador.
     */
    @Test
    public void testSetAndGetComprimentoFibra() {
        System.out.println("setAndGetComprimentoFibra");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();

        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));

        instance.setComprimentoFibra(10);
        assertTrue("O resultado deveria ser 10", instance.getComprimentoFibra() == 10);

        instance.setComprimentoFibra(30);
        assertTrue("O resultado deveria ser 30", instance.getComprimentoFibra() == 30);

    }

    /**
     * Test of setAndGetAnguloIncidencia method, of class Simulador.
     */
    @Test
    public void testSetAndaGetAnguloIncidencia() {
        System.out.println("setAndgetAnguloIncidencia");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();

        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(10);
        assertTrue("O resultado deveria ser 10", instance.getAnguloIncidencia() == 10);

        instance.setAnguloIncidencia(20);
        assertTrue("O resultado deveria ser 20", instance.getAnguloIncidencia() == 20);

    }

    /**
     * Test of getComprimentoOnda method, of class Simulador.
     */
    @Test
    public void testSetAndGetComprimentoOnda() {
        System.out.println("setAndGetComprimentoOnda");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();

        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));

        instance.setComprimentoOnda(700f);
        assertTrue("O resultado deveria ser 700", instance.getComprimentoOnda() == 700f);

        instance.setComprimentoOnda(200);
        assertTrue("O resultado deveria ser 200", instance.getComprimentoOnda() == 200f);

    }

    /**
     * Test of validaDados method, of class Simulador.
     */
    @Test
    public void testValidaDados() {
        System.out.println("validaDados");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));

        assertFalse("Deveria dar falso", instance.validaDados());

        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(10);
        instance.setPotenciaEntrada(100);
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-alto indice", 1.9f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        instance.validaDados();
        assertTrue("O resultado deveria ser true", instance.validaDados());
    }

    /**
     * Test of calcularAnguloMaximoAceitacao method, of class Simulador.
     */
    @Test
    public void testCalcularAnguloMaximoAceitacao() {
        System.out.println("calcularAnguloMaximoAceitacao");
        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-baixo indice", 1.5f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(10);
        instance.setPotenciaEntrada(100);

        double expResult = 9.957f;
        double result = instance.calcularAnguloMaximoAceitacao();
        assertEquals(expResult, result, 0.4f);

        Bainha bainha2 = new Bainha("Glicerina", 1.47f);
        fo.setBainha(bainha2);
        expResult = 17.37f;
        result = instance.calcularAnguloMaximoAceitacao();
        assertEquals(expResult, result, 0.4f);

    }

    /**
     * Test of calcularConeAceitacao method, of class Simulador.
     */
    @Test
    public void testCalcularConeAceitacao() {
        System.out.println("calcularConeAceitacao");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-baixo indice", 1.5f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(10);
        instance.setPotenciaEntrada(100);

        double anguloAceitacao = instance.calcularAnguloMaximoAceitacao();
        double expResult = 2 * anguloAceitacao;
        double result = instance.calcularConeAceitacao();
        assertEquals(expResult, result, 0.4f);

        Bainha bainha2 = new Bainha("Glicerina", 1.47f);
        fo.setBainha(bainha2);
        anguloAceitacao = instance.calcularAnguloMaximoAceitacao();
        expResult = 2 * anguloAceitacao;
        result = instance.calcularConeAceitacao();
        assertEquals(expResult, result, 0.4f);

    }

    /**
     * Test of calcularAberturaNumerica method, of class Simulador.
     */
    @Test
    public void testAberturaNumerica() {
        System.out.println("aberturaNumerica");
        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(10);
        instance.setPotenciaEntrada(100);

        double expResult = 0.243311f;
        double result = instance.calcularAberturaNumerica();
        assertEquals(expResult, result, 0.4f);
        nucleo = new Nucleo("Vidro-baixo indice", 1.50f);
        Bainha bainha2 = new Bainha("Glicerina", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha2);

        expResult = 0.172916f;
        result = instance.calcularAberturaNumerica();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularPotenciaSaida method, of class Simulador.
     */
    @Test
    public void testPotenciaSaida() {
        System.out.println("potenciaSaida");
        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(20);
        instance.setPotenciaEntrada(0.001f);
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-baixo indice", 1.5f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        double expResult = 0.000969f;
        double result = instance.calcularPotenciaSaida();
        assertEquals(expResult, result, 0.9f);
    }

    /**
     * Test of calcularAnguloCritico method, of class Simulador.
     */
    @Test
    public void testAnguloCritico() {
        System.out.println("anguloCritico");
        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(10);
        instance.setPotenciaEntrada(100);

        double expResult = 80.6;
        double result = instance.calcularAnguloCritico();
        assertEquals(expResult, result, 0.4f);

    }

    /**
     * Test of setAndGetDiametroNucleo method, of class Simulador.
     */
    @Test
    public void testSetAndGetDiametroNucleo() {
        System.out.println("setAndGetDiametroNucleo");

        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        float valo = instance.getDiametroNucleo();
        assertTrue("O valor esperado é 0.000008f", instance.getDiametroNucleo() == 0.000008f);

        instance.setDiametroNucleo(0.000125f);
        assertTrue("O valor esperado é 0.000125f", instance.getDiametroNucleo() == 0.000125f);
    }

    /**
     * Test of calcularFrequenciaNormalizada o method, of class Simulador.
     */
    @Test
    public void testcalcularFrequenciaNormalizada() {
        System.out.println("calcularFrequenciaNormalizada");
        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(10);
        instance.setPotenciaEntrada(100);
        instance.calcularAberturaNumerica();
        double expectedValue = 3.057528e-8f;
        double result = instance.calcularFrequenciaNormalizada();

        assertEquals("A frequência normalizada esperada é  3.057528", expectedValue, result, 1e15f);
    }

    /**
     * Test of classificaFibra o method, of class Simulador.
     */
    @Test
    public void testClassificaFibra() {
        System.out.println("classificaFibra");
        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        Simulador instance = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"));
        instance.setAnguloIncidencia(20);
        instance.setComprimentoFibra(10);
        instance.setPotenciaEntrada(100);
        instance.calcularAberturaNumerica();
        double freqNormalizada = instance.calcularFrequenciaNormalizada();
        assertEquals("O valor esperado é que seja Monomodo", "Monomodo", instance.classificaFibra());
    }
}
