/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class BainhaTest {
    
    public BainhaTest() {
    }

    /**
     * Test of getNomeMaterial method and setNomeMaterial, of class Bainha.
     */
    @Test
    public void testGetSetNomeMaterial() {
        System.out.println("getSetNomeMaterial");
        Bainha instance = new Bainha();
        String expResult = "nome";
        instance.setNomeMaterial(expResult);
        String result = instance.getNomeMaterial();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIndiceMaterial method and setIndice method, of class Bainha.
     */
    @Test
    public void testGetSetIndiceMaterial() {
        System.out.println("getSetIndiceMaterial");
        Bainha instance = new Bainha();
        float expResult = 1.4F;
        instance.setIndiceMaterial(expResult);
        float result = instance.getIndiceMaterial();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class Bainha.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Bainha instance = new Bainha();
        instance.setNomeMaterial("nome");
        instance.setIndiceMaterial(1.4F);
        String expResult = "Nome da bainha: nome\n Índice da bainha: " + 1.4F;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Bainha.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Bainha obj = new Bainha();
        Bainha instance = new Bainha();
        float indice = 1.4F;
        String nome = "nome";
        obj.setIndiceMaterial(indice);
        obj.setNomeMaterial(nome);
        instance.setIndiceMaterial(indice);
        instance.setNomeMaterial(nome);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Bainha.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equalsNot");
        Bainha obj = new Bainha();
        Bainha instance = new Bainha();
        float indice = 1.4F;
        String nome = "nome";
        String nome2 = "nome2";
        obj.setIndiceMaterial(indice);
        obj.setNomeMaterial(nome);
        instance.setIndiceMaterial(indice);
        instance.setNomeMaterial(nome2);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }   
}
