/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class SimuladorComDeformacaoTest {

    FibraOtica fo = new FibraOtica();
    ComprimentoOnda compOnda = new ComprimentoOnda();
    DiametroNucleo diametroNucleo = new DiametroNucleo();
    float distanciaSemDeformacao = 5;
    float variacaoDiametroNucleo = 2f;
    float compFibra = 10f;

    public SimuladorComDeformacaoTest() {
    }

    /**
     * Test of setAnguloIncidencia method, of class SimuladorComDeformacao.
     */
    @Test
    public void testSetAnguloIncidencia() {
        System.out.println("setAnguloIncidencia");
        System.out.println("setComprimentoOnda");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(5);
        assertTrue("O resultado deveria ser true", instance.getAnguloIncidencia() == 5);

        instance.setAnguloIncidencia(5);
        instance.setAnguloIncidencia(10);
        assertTrue("O resultado deveria ser true", instance.getAnguloIncidencia() == 10);
    }

    /**
     * Test of setComprimentoFibra method, of class SimuladorComDeformacao.
     */
    @Test
    public void testSetComprimentoFibra() {
        System.out.println("setComprimentoFibra");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        assertTrue("O resultado deveria ser true", instance.getComprimentoFibra() == 10);

        instance.setComprimentoFibra(15);
        instance.setComprimentoFibra(5);
        assertTrue("O resultado deveria ser true", instance.getComprimentoFibra() == 5);
    }

    /**
     * Test of setPotenciaEntrada method, of class SimuladorComDeformacao.
     */
    @Test
    public void testSetPotenciaEntrada() {
        System.out.println("setPotenciaEntrada");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setPotenciaEntrada(10);
        assertTrue("O resultado deveria ser true", instance.getPotenciaEntrada() == 10);

        instance.setPotenciaEntrada(15);
        instance.setPotenciaEntrada(5);
        assertTrue("O resultado deveria ser true", instance.getPotenciaEntrada() == 5);
    }

    /**
     * Test of setDistanciaSemDeformacao method, of class
     * SimuladorComDeformacao.
     */
    @Test
    public void testSetDistanciaSemDeformacao() {
        System.out.println("setDistanciaSemDeformacao");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setComprimentoFibra(10);
        instance.setDistanciaSemDeformacao(10);
        assertFalse("O resultado deveria ser false", instance.getDistanciaSemDeformacao() == 10);

        instance.setDistanciaSemDeformacao(15);
        assertFalse("O resultado deveria ser false", instance.getDistanciaSemDeformacao() == 15);

        instance.setDistanciaSemDeformacao(5);
        assertTrue("O resultado deveria ser true", instance.getDistanciaSemDeformacao() == 5);
    }

    /**
     * Test of setVariacaoDiametroNucleo method, of class
     * SimuladorComDeformacao.
     */
    @Test
    public void testSetVariacaoDiametroNucleo() {
        System.out.println("setVariacaoDiametroNucleo");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setVariacaoDiametroNucleo(10);
        assertFalse("O resultado deveria ser false", instance.getVariacaoDiametroNucleo() == 10);

        instance.setVariacaoDiametroNucleo(15);
        assertFalse("O resultado deveria ser false", instance.getVariacaoDiametroNucleo() == 15);

        instance.setVariacaoDiametroNucleo(5);
        assertTrue("O resultado deveria ser true", instance.getVariacaoDiametroNucleo() == 0.000005f);
    }

    /**
     * Test of criarSimuladorComDeformacao method, of class
     * SimuladorComDeformacao.
     */
    @Test
    public void testCriarSimuladorComDeformacao() {
        System.out.println("criarSimuladorComDeformacao");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        assertFalse("O resultado deveria ser falso", instance.criarSimuladorComDeformacao());

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-alto indice", 1.9f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        instance.criarSimuladorComDeformacao();
        assertTrue("O resultado deveria ser verdadeiro", instance.criarSimuladorComDeformacao());
    }

    /**
     * Test of calcularAnguloMaximoAceitacao method, of class
     * SimuladorComDeformacao.
     */
    @Test
    public void testCalcularAnguloMaximoAceitacao() {
        System.out.println("calcularAnguloMaximoAceitacao");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-baixo indice", 1.5f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        double expResult = 0.17379f;
        instance.criarSimuladorComDeformacao();
        double result = instance.calcularAnguloMaximoAceitacao();
        assertEquals(expResult, result, 0.4f);

        Bainha bainha2 = new Bainha("Glicerina", 1.47f);
        fo.setBainha(bainha2);
        expResult = 0.303116f;
        result = instance.calcularAnguloMaximoAceitacao();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularConeAceitacao method, of class SimuladorComDeformacao.
     */
    @Test
    public void testCalcularConeAceitacao() {
        System.out.println("calcularConeAceitacao");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-baixo indice", 1.5f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        instance.criarSimuladorComDeformacao();

        double anguloAceitacao = instance.calcularAnguloMaximoAceitacao();
        double expResult = 2 * anguloAceitacao;

        double result = instance.calcularConeAceitacao();
        assertEquals(expResult, result, 0.4f);

        Bainha bainha2 = new Bainha("Glicerina", 1.47f);
        fo.setBainha(bainha2);
        anguloAceitacao = instance.calcularAnguloMaximoAceitacao();
        expResult = 2 * anguloAceitacao;
        result = instance.calcularConeAceitacao();
        assertEquals(expResult, result, 0.4f);

    }

    /**
     * Test of calcularAberturaNumerica method, of class SimuladorComDeformacao.
     */
    @Test
    public void testCalcularAberturaNumerica() {
        System.out.println("calcularAberturaNumerica");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        instance.criarSimuladorComDeformacao();
        double expResult = 0.243311f;
        double result = instance.calcularAberturaNumerica();
        assertEquals(expResult, result, 0.4f);
        nucleo = new Nucleo("Vidro-baixo indice", 1.50f);
        Bainha bainha2 = new Bainha("Glicerina", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha2);

        expResult = 0.172916f;
        result = instance.calcularAberturaNumerica();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularPotenciaSaida method, of class SimuladorComDeformacao.
     */
    @Test
    public void testCalcularPotenciaSaida() {
        System.out.println("calcularPotenciaSaida");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        instance.criarSimuladorComDeformacao();
        double expResult = 100f;
        double result = instance.calcularPotenciaSaida();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularAnguloCritico method, of class SimuladorComDeformacao.
     */
    @Test
    public void testCalcularAnguloCritico() {
        System.out.println("calcularAnguloCritico");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        instance.criarSimuladorComDeformacao();
        double expResult = 1.406765f;
        double result = instance.calcularAnguloCritico();
        assertEquals(expResult, result, 0.4f);
    }

    /**
     * Test of calcularFrequenciaNormalizada method, of class
     * SimuladorComDeformacao.
     */
    @Test
    public void testCalcularFrequenciaNormalizada() {
        System.out.println("calcularFrequenciaNormalizada");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        instance.criarSimuladorComDeformacao();
        instance.calcularAberturaNumerica();
        double expectedValue = 3.057528e-8f;
        double result = instance.calcularFrequenciaNormalizada();

        assertEquals("A frequência normalizada esperada é  3.057528", expectedValue, result, 1e15f);

    }

    /**
     * Test of classificaFibra method, of class SimuladorComDeformacao.
     */
    @Test
    public void testClassificaFibra() {
        System.out.println("classificaFibra");
        diametroNucleo.preencheListaDiametroNucleo();
        compOnda.preencheListaComprimentoOnda();
        Bainha bainha = new Bainha("Glicerina", 1.47f);
        Nucleo nucleo = new Nucleo("Acrilico", 1.49f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        SimuladorComDeformacao instance = new SimuladorComDeformacao(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Laranja"), distanciaSemDeformacao, compFibra);

        instance.setAnguloIncidencia(20);
        instance.setPotenciaEntrada(100);
        instance.criarSimuladorComDeformacao();
        instance.calcularAberturaNumerica();
        double freqNormalizada = instance.calcularFrequenciaNormalizada();
        assertEquals("Monomodo", instance.classificaFibra());
    }

}
