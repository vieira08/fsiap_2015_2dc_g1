/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class FibraOticaTest {

    public FibraOticaTest() {
    }

       
    /**
     * Test of setNucleo method, of class FibraOtica.
     */
    @Test
    public void testSetAndGetNucleo() {
        System.out.println("setAndGetNucleo");
        Nucleo nucleo = new Nucleo("Vidro", 1.5f);
        FibraOtica instance = new FibraOtica();
        instance.setNucleo(nucleo);
        assertEquals(nucleo, instance.getNucleo());
    }

    /**
     * Test of setBainha method, of class FibraOtica.
     */
    @Test
    public void testSetAndGetBainha() {
        System.out.println("setAndGetBainha");
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro", 1.5f);
        FibraOtica instance = new FibraOtica(nucleo, bainha);
        assertEquals(bainha, instance.getBainha());

    }

      /**
     * Test of setBainha method, of class FibraOtica.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testSetGetBainha1() {
        System.out.println("setBainha");
        Bainha bainha = new Bainha("Acrilico", 1.5f);
        Nucleo nucleo = new Nucleo("Vidro", 1.49f);
        FibraOtica instance = new FibraOtica(nucleo, bainha);
        instance.setBainha(bainha);
    }
    
    /**
     * Test of equals method, of class FibraOtica.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro", 1.5f);
        Object obj = new FibraOtica(nucleo, bainha);
        FibraOtica instance = new FibraOtica(nucleo, bainha);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals1() {
        System.out.println("equals");
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro", 1.5f);
        Bainha bainha2 = new Bainha("Alcool", 1.3f);
        Object obj = new FibraOtica(nucleo, bainha);
        FibraOtica instance = new FibraOtica(nucleo, bainha2);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class FibraOtica.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro", 1.5f);
        FibraOtica instance = new FibraOtica(nucleo,bainha);
        String expResult =  "Indice de refração do "+nucleo.getNomeMaterial()+"(nucleo): "+nucleo.getIndiceMaterial()+"\n"
                +"Indice de refração do "+bainha.getNomeMaterial()+"(bainha): "+bainha.getIndiceMaterial();;
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
