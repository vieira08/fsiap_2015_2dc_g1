/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class GestorSimuladoresTest {

    private ArrayList<Simulador> instance = new ArrayList<Simulador>();

    public GestorSimuladoresTest() {
    }

    /**
     * Test of getInstance method, of class GestorSimuladores.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        GestorSimuladores result = GestorSimuladores.getInstance();
        assertFalse("O resultado deveria ser diferente de null", result == null);

    }

    /**
     * Test of getListaSimuladores method, of class GestorSimuladores.
     */
    @Test
    public void testGetListaSimuladores() {
        System.out.println("getListaSimuladores");
        GestorSimuladores gestor = GestorSimuladores.getInstance();

        assertTrue("O resultado deveria ser 0", gestor.getListaSimuladores().size() == instance.size());

        ComprimentoOnda compOnda = new ComprimentoOnda();
        DiametroNucleo diametroNucleo = new DiametroNucleo();
        FibraOtica fo = new FibraOtica();
        compOnda.preencheListaComprimentoOnda();
        diametroNucleo.preencheListaDiametroNucleo();
        Bainha bainha = new Bainha("Acrilico", 1.49f);
        Nucleo nucleo = new Nucleo("Vidro-baixo indice", 1.5f);
        fo.setNucleo(nucleo);
        fo.setBainha(bainha);
        Simulador simulador = new Simulador(fo, diametroNucleo.getDiametroNucleo("8"),
                            compOnda.getComprimentoOnda("Visivel-Amarelo"));

        gestor.getListaSimuladores().add(simulador);
        assertFalse("O resultado deveria ser falso", gestor.getListaSimuladores().size() == instance.size());
    }

}
