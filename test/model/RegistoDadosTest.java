/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author G01
 */
public class RegistoDadosTest {

    private RegistoDados registoDados;

    public RegistoDadosTest() {
        this.registoDados = RegistoDados.getInstance();
        this.registoDados.preencherListas();
    }

    /**
     * Test of getListaMateriaisNucleo method, of class RegistoDados.
     */
    @Test
    public void testGetListaMateriaisNucleo() {
        System.out.println("getListaMateriaisNucleo");
        RegistoDados instance = this.registoDados;
        HashMap<String, Float> listaMaterial = new HashMap<>();
        listaMaterial.put("Vidro-baixo indice", 1.5f);
        listaMaterial.put("Vidro-alto indice", 1.9f);
        listaMaterial.put("Silex vidro", 1.62f);
        listaMaterial.put("Vidro ótico", 1.54f);
        listaMaterial.put("Glicerina", 1.47f);
        listaMaterial.put("Policarbonato", 1.59f);
        listaMaterial.put("Silica", 1.458f);
        listaMaterial.put("Acrilico", 1.49f);
        Set<String> expResult = listaMaterial.keySet();
        Set<String> result = instance.getListaMateriaisNucleo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaMateriaisBainha method, of class RegistoDados.
     */
    @Test
    public void testGetListaMateriaisBainha() {
        System.out.println("getListaMateriaisBainha");
        float indice = 1.49f;
        RegistoDados instance = this.registoDados;
        instance.getListaMateriaisNucleo();
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Glicerina");
        ArrayList<String> result = instance.getListaMateriaisBainha(indice);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaComprimentoOnda method, of class RegistoDados.
     */
    @Test
    public void testGetListaComprimentoOnda() {
        System.out.println("getListaComprimentoOnda");
        RegistoDados instance = this.registoDados;
        float indice = 1.49f;
        instance.getListaMateriaisNucleo();
        instance.getListaMateriaisBainha(indice);
        HashMap<String, Float> listaComprimentoOnda = new HashMap<>();
        listaComprimentoOnda.put("Visivel-Amarelo", 0.580f);
        listaComprimentoOnda.put("Visivel-Vermelho", 0.660f);
        listaComprimentoOnda.put("Visivel-Laranja", 0.580f);
        listaComprimentoOnda.put("Visivel-Verde", 0.533f);
        listaComprimentoOnda.put("Visivel-Violeta", 0.425f);
        listaComprimentoOnda.put("Visivel-Azul", 0.485f);
        Set<String> expResult = listaComprimentoOnda.keySet();
        Set<String> result = instance.getListaComprimentoOnda();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaDiametroNucleo method, of class RegistoDados.
     */
    @Test
    public void testGetListaDiametroNucleo() {
        System.out.println("getListaDiametroNucleo");
        RegistoDados instance = this.registoDados;
        float indice = 1.49f;
        instance.getListaMateriaisNucleo();
        instance.getListaMateriaisBainha(indice);
        instance.getListaComprimentoOnda();
        HashMap<String, Float> listaDiametroNucleo = new HashMap<>();
        listaDiametroNucleo.put("5", 0.000005f);
        listaDiametroNucleo.put("6", 0.000006f);
        listaDiametroNucleo.put("7", 0.000007f);
        listaDiametroNucleo.put("8", 0.000008f);
        listaDiametroNucleo.put("9", 0.000009f);
        listaDiametroNucleo.put("10", 0.000010f);
        Set<String> expResult = listaDiametroNucleo.keySet();
        Set<String> result = instance.getListaDiametroNucleo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getComprimentoOnda method, of class RegistoDados.
     */
    @Test
    public void testGetComprimentoOnda() {
        System.out.println("getComprimentoOnda");
        String comprimentoOnda = "Visivel-Verde";
        RegistoDados instance = this.registoDados;
        float indice = 1.49f;
        instance.getListaMateriaisNucleo();
        instance.getListaMateriaisBainha(indice);
        instance.getListaComprimentoOnda();
        instance.getListaDiametroNucleo();
        float expResult = 0.533f;
        float result = instance.getComprimentoOnda(comprimentoOnda);
        assertEquals(expResult, result, 0.3f);
    }

    /**
     * Test of getDiametroNucleo method, of class RegistoDados.
     */
    @Test
    public void testGetDiametroNucleo() {
        System.out.println("getDiametroNucleo");

        RegistoDados instance = this.registoDados;
        float indice = 1.49f;
        String diametro = "5";
        String comprimentoOnda = "Visivel-Verde";
        instance.getListaMateriaisNucleo();
        instance.getListaMateriaisBainha(indice);
        instance.getListaComprimentoOnda();
        instance.getListaDiametroNucleo();
        instance.getComprimentoOnda(comprimentoOnda);
        float expResult = 0.000005f;
        float result = instance.getDiametroNucleo(diametro);
        assertEquals(expResult, result, 0.7f);
    }

    /**
     * Test of getIndiceNucleo method, of class RegistoDados.
     */
    @Test
    public void testGetIndiceNucleo() {
        System.out.println("getIndiceNucleo");
        String material = "Policarbonato";
        RegistoDados instance = this.registoDados;
        float indice = 1.49f;
        String diametro = "6";
        String comprimentoOnda = "Visivel-Verde";
        instance.getListaMateriaisNucleo();
        instance.getListaMateriaisBainha(indice);
        instance.getListaComprimentoOnda();
        instance.getListaDiametroNucleo();
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getDiametroNucleo(diametro);
        float expResult = 1.59f;
        float result = instance.getIndiceNucleo(material);
        assertEquals(expResult, result, 0.0f);
    }

    /**
     * Test of getIndiceBainha method, of class RegistoDados.
     */
    @Test
    public void testGetIndiceBainha() {
        System.out.println("getIndiceBainha");
        String material = "Vidro ótico";
        RegistoDados instance = this.registoDados;
        float indice = 1.49f;
        String diametro = "7";
        String comprimentoOnda = "Visivel-Verde";
        instance.getListaMateriaisNucleo();
        instance.getListaMateriaisBainha(indice);
        instance.getListaComprimentoOnda();
        instance.getListaDiametroNucleo();
        instance.getComprimentoOnda(comprimentoOnda);
        instance.getDiametroNucleo(diametro);
        instance.getIndiceNucleo(material);
        float expResult = 1.54f;
        float result = instance.getIndiceBainha(material);
        assertEquals(expResult, result, 0.0f);
    }

    /**
     * Test of getInstance method, of class RegistoDados.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        RegistoDados expResult = this.registoDados;
        RegistoDados result = RegistoDados.getInstance();
        assertEquals(expResult, result);
    }

    /**
     * Test of preencherListas method, of class RegistoDados.
     */
    @Test
    public void testPreencherListas() {
        System.out.println("preencherListas");
        RegistoDados instance = this.registoDados;
        instance.preencherListas();
    }
}
