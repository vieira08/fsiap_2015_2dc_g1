/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Frame;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.GestorSimuladores;
import model.Simulador;

/**
 *
 * @author G01
 */
public class MostrarSimulacoes extends javax.swing.JDialog {

    private Frame framePai;

    /**
     * Creates new form MostrarSimulacoes
     */
    public MostrarSimulacoes(java.awt.Frame parent, boolean modal) {
        super(parent, "Mostrar Simulações", modal);
        this.framePai = parent;
        setResizable(false);
        initComponents();
        setLocationRelativeTo(null);
        preencherTabela();
        setVisible(true);
        pack();
    }

    private void preencherTabela() {
        ArrayList<Simulador> gestorSimuladores = GestorSimuladores.getInstance().getListaSimuladores();
        int numSimuladores = gestorSimuladores.size();
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        //model.addColumn("Simul");
        if (numSimuladores > 0) {
            String dadosSimulacao = java.util.ResourceBundle.getBundle("ui/Bundle").getString("DadosSimulacao");
            model.addColumn(dadosSimulacao);
            
            String[][] data = new String[11][numSimuladores + 1];
            
            String potenciaEntrada = java.util.ResourceBundle.getBundle("ui/Bundle").getString("potenciaEntrada");
            data[0][0] = potenciaEntrada; 
            
            String anguloIncidencia = java.util.ResourceBundle.getBundle("ui/Bundle").getString("anguloIncidencia");
            data[1][0] = anguloIncidencia;
            
            String comprimentoFibra = java.util.ResourceBundle.getBundle("ui/Bundle").getString("comprimentoFibra");
            data[2][0] = comprimentoFibra;
            
            String comprimentoOnda = java.util.ResourceBundle.getBundle("ui/Bundle").getString("comprimentoOnda");
            data[3][0] = comprimentoOnda;
            
            String diametroNucleo = java.util.ResourceBundle.getBundle("ui/Bundle").getString("diametroNucleo");
            data[4][0] = diametroNucleo;
            
            String anguloMaximoAceitacao = java.util.ResourceBundle.getBundle("ui/Bundle").getString("anguloMaximoAceitacao");
            data[5][0] = anguloMaximoAceitacao;
            
            String frequenciaNormalizada = java.util.ResourceBundle.getBundle("ui/Bundle").getString("frequenciaNormalizada");
            data[6][0] = frequenciaNormalizada;
            
            String coneAceitacao = java.util.ResourceBundle.getBundle("ui/Bundle").getString("coneAceitacao");
            data[7][0] = coneAceitacao;
            
            String potenciaSaida = java.util.ResourceBundle.getBundle("ui/Bundle").getString("potenciaSaida");
            data[8][0] = potenciaSaida;
            
            String anguloCritico = java.util.ResourceBundle.getBundle("ui/Bundle").getString("anguloCritico");
            data[9][0] = anguloCritico;
            
            String classificacaoFibra = java.util.ResourceBundle.getBundle("ui/Bundle").getString("classificacaoFibra");
            data[10][0] = classificacaoFibra;

            String simulador1 = java.util.ResourceBundle.getBundle("ui/Bundle").getString("simulador");
            for (int coluna = 0; coluna < numSimuladores; coluna++) {
                model.addColumn(simulador1 + (coluna + 1));
                int index = coluna + 1;
                Simulador simulador = gestorSimuladores.get(coluna);
                data[0][index] = String.valueOf(simulador.getPotenciaEntrada());
                data[1][index] = String.valueOf(simulador.getAnguloIncidencia());
                data[2][index] = String.valueOf(simulador.getComprimentoFibra());
                data[3][index] = String.valueOf(simulador.getComprimentoOnda());
                data[4][index] = String.valueOf(simulador.getDiametroNucleo());
                data[5][index] = String.valueOf(simulador.getAnguloMaxAceitacao());
                data[6][index] = String.valueOf(simulador.getFreqNormalizada());
                data[7][index] = String.valueOf(simulador.getConeAceitacao());
                data[8][index] = String.valueOf(simulador.getPotenciaSaida());
                data[9][index] = String.valueOf(simulador.getAnguloCritico());
                data[10][index] = simulador.getClassificaFibra();
            }
            for (int i = 0; i < data.length; i++) {
                model.addRow(data[i]);
            }
        } else {
            String semSimulacoesMostrar = java.util.ResourceBundle.getBundle("ui/Bundle").getString("semSimulacoesMostrar");
            throw new IllegalArgumentException(semSimulacoesMostrar);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("ui/Bundle"); // NOI18N
        setTitle(bundle.getString("simulationsShow")); // NOI18N

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        table.setRowHeight(32);
        jScrollPane2.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 962, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
