/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.SimularTransmissaoComDeformacaoController;
import java.awt.Frame;
import java.util.List;
import javax.swing.JOptionPane;
import model.SimuladorComDeformacao;

/**
 *
 * @author Beatriz
 */
public class SimularTransmissaoDeformacaoUI extends javax.swing.JDialog {

    private Frame framePai;
    private SimularTransmissaoComDeformacaoController controller;
    private SimuladorComDeformacao simulador;
    private String str;

    /**
     * Creates new form SimularTransmissaoDeformacaoUI
     */
    public SimularTransmissaoDeformacaoUI(java.awt.Frame parent, boolean modal) {
        super(parent, "Simular Transmisão", modal);
        this.controller = new SimularTransmissaoComDeformacaoController();
        this.framePai = parent;
        this.str = java.util.ResourceBundle.getBundle("ui/Bundle").getString("SimularTransmisãoDeformacao");
        setResizable(false);
        initComponents();
        setLocationRelativeTo(null);
        setVisible(true);
        pack();
        getRootPane().setDefaultButton(this.btn_selecionarNucleo);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        cmb_selecionarNucleo = new javax.swing.JComboBox(this.controller.getListaMateriaisNucleo().toArray());
        btn_selecionarNucleo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        cmb_selecionarBainha = new javax.swing.JComboBox();
        cmb_selecionarBainha.setEnabled(false);
        btn_selecionarBainha = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        cmb_selecionarComprimentoOnda = new javax.swing.JComboBox(this.controller.getListaComprimentoOnda().toArray());
        cmb_selecionarComprimentoOnda.setEnabled(false);
        btn_selecionarComprimentoOnda = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        cmb_selecionarDiametroNucleo = new javax.swing.JComboBox(this.controller.getListaDiametroNucleo().toArray());
        cmb_selecionarDiametroNucleo.setEnabled(false);
        btn_selecionarDiametroNucleo = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        lbl_potenciaEntrada = new javax.swing.JLabel();
        txt_potenciaEntrada = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txt_anguloIncidencia = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_comprimentoFibra = new javax.swing.JTextField();
        lbl_distanciaDeformacao = new javax.swing.JLabel();
        txt_distanciaDeformacao = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txt_variacaoNucleo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btn_simular = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("ui/Bundle"); // NOI18N
        setTitle(bundle.getString("SimularTransmisãoDeformacao")); // NOI18N

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("SimularTransmissaoDeformacaoUI.jPanel2.border.title"))); // NOI18N

        cmb_selecionarNucleo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_selecionarNucleoActionPerformed(evt);
            }
        });

        btn_selecionarNucleo.setText(bundle.getString("SimularTransmissaoDeformacaoUI.btn_selecionarNucleo.text")); // NOI18N
        btn_selecionarNucleo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_selecionarNucleoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmb_selecionarNucleo, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_selecionarNucleo)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_selecionarNucleo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_selecionarNucleo))
                .addGap(0, 16, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("SimularTransmissaoDeformacaoUI.jPanel1.border.title"))); // NOI18N

        cmb_selecionarBainha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_selecionarBainhaActionPerformed(evt);
            }
        });

        btn_selecionarBainha.setText(bundle.getString("SimularTransmissaoDeformacaoUI.btn_selecionarBainha.text")); // NOI18N
        btn_selecionarBainha.setEnabled(false);
        btn_selecionarBainha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_selecionarBainhaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmb_selecionarBainha, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_selecionarBainha)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_selecionarBainha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_selecionarBainha))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("SimularTransmissaoDeformacaoUI.jPanel3.border.title"))); // NOI18N

        cmb_selecionarComprimentoOnda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_selecionarComprimentoOndaActionPerformed(evt);
            }
        });

        btn_selecionarComprimentoOnda.setText(bundle.getString("SimularTransmissaoDeformacaoUI.btn_selecionarComprimentoOnda.text")); // NOI18N
        btn_selecionarComprimentoOnda.setEnabled(false);
        btn_selecionarComprimentoOnda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_selecionarComprimentoOndaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmb_selecionarComprimentoOnda, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_selecionarComprimentoOnda)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_selecionarComprimentoOnda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_selecionarComprimentoOnda))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("SimularTransmissaoDeformacaoUI.jPanel4.border.title"))); // NOI18N

        cmb_selecionarDiametroNucleo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_selecionarDiametroNucleoActionPerformed(evt);
            }
        });

        btn_selecionarDiametroNucleo.setText(bundle.getString("SimularTransmissaoDeformacaoUI.btn_selecionarDiametroNucleo.text")); // NOI18N
        btn_selecionarDiametroNucleo.setEnabled(false);
        btn_selecionarDiametroNucleo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_selecionarDiametroNucleoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmb_selecionarDiametroNucleo, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_selecionarDiametroNucleo)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_selecionarDiametroNucleo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_selecionarDiametroNucleo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("SimularTransmissaoDeformacaoUI.jPanel5.border.title"))); // NOI18N

        lbl_potenciaEntrada.setText(bundle.getString("SimularTransmissaoDeformacaoUI.lbl_potenciaEntrada.text")); // NOI18N

        this.txt_distanciaDeformacao.setEditable(false);;
        txt_potenciaEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_potenciaEntradaActionPerformed(evt);
            }
        });

        jLabel1.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel1.text")); // NOI18N

        this.txt_distanciaDeformacao.setEditable(false);
        txt_anguloIncidencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_anguloIncidenciaActionPerformed(evt);
            }
        });

        jLabel2.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel2.text")); // NOI18N

        this.txt_distanciaDeformacao.setEditable(false);
        txt_comprimentoFibra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_comprimentoFibraActionPerformed(evt);
            }
        });

        lbl_distanciaDeformacao.setText(bundle.getString("SimularTransmissaoDeformacaoUI.lbl_distanciaDeformacao.text")); // NOI18N

        this.txt_distanciaDeformacao.setEditable(false);
        txt_distanciaDeformacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_distanciaDeformacaoActionPerformed(evt);
            }
        });

        jLabel3.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel3.text")); // NOI18N

        this.txt_distanciaDeformacao.setEditable(false);
        txt_variacaoNucleo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_variacaoNucleoActionPerformed(evt);
            }
        });

        jLabel4.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel4.text_1")); // NOI18N

        jLabel5.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel5.text_1")); // NOI18N

        jLabel6.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel6.text_1")); // NOI18N

        jLabel7.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel7.text_1")); // NOI18N

        jLabel8.setText(bundle.getString("SimularTransmissaoDeformacaoUI.jLabel8.text_1")); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lbl_distanciaDeformacao)
                        .addComponent(jLabel3))
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(txt_anguloIncidencia, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(txt_variacaoNucleo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbl_potenciaEntrada))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(txt_distanciaDeformacao, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(txt_potenciaEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(txt_comprimentoFibra, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)))
                .addGap(40, 40, 40))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_distanciaDeformacao)
                    .addComponent(txt_distanciaDeformacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txt_comprimentoFibra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_variacaoNucleo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_potenciaEntrada)
                    .addComponent(txt_potenciaEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_anguloIncidencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel6))
                .addContainerGap())
        );

        btn_simular.setText(bundle.getString("SimularTransmissaoDeformacaoUI.btn_simular.text")); // NOI18N
        btn_simular.setEnabled(false);
        btn_simular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simularActionPerformed(evt);
            }
        });

        btn_cancelar.setText(bundle.getString("SimularTransmissaoDeformacaoUI.btn_cancelar.text")); // NOI18N
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_simular, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simular)
                    .addComponent(btn_cancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmb_selecionarNucleoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_selecionarNucleoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_selecionarNucleoActionPerformed

    private void btn_selecionarNucleoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_selecionarNucleoActionPerformed
        String materialNucleo = this.cmb_selecionarNucleo.getSelectedItem().toString();
        this.controller.getMaterialNucleo(materialNucleo);
        List<String> listaMateriaisBainha = this.controller.getListaMateriaisBainha();

        try {
            if (!listaMateriaisBainha.isEmpty()) {
                for (String string : listaMateriaisBainha) {
                    this.cmb_selecionarBainha.addItem(string);
                }
                this.cmb_selecionarNucleo.setEnabled(false);
                this.btn_selecionarNucleo.setEnabled(false);
                this.cmb_selecionarBainha.setEnabled(true);
                this.btn_selecionarBainha.setEnabled(true);
                getRootPane().setDefaultButton(this.btn_selecionarBainha);
            } else {
                String str1 = java.util.ResourceBundle.getBundle("ui/Bundle").getString("semMaterialBainha");
                throw new IllegalArgumentException(str1);
            }
        } catch (IllegalArgumentException e) {
            JOptionPane.showMessageDialog(
                                framePai,
                                e.getMessage(),
                                this.str,
                                JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_selecionarNucleoActionPerformed

    private void cmb_selecionarBainhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_selecionarBainhaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_selecionarBainhaActionPerformed

    private void btn_selecionarBainhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_selecionarBainhaActionPerformed
        String materialBainha = this.cmb_selecionarBainha.getSelectedItem().toString();
        this.controller.getMaterialBainha(materialBainha);
        this.controller.createFO();

        this.cmb_selecionarBainha.setEnabled(false);
        this.btn_selecionarBainha.setEnabled(false);
        this.cmb_selecionarComprimentoOnda.setEnabled(true);
        this.btn_selecionarComprimentoOnda.setEnabled(true);
        getRootPane().setDefaultButton(this.btn_selecionarComprimentoOnda);
    }//GEN-LAST:event_btn_selecionarBainhaActionPerformed

    private void cmb_selecionarComprimentoOndaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_selecionarComprimentoOndaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_selecionarComprimentoOndaActionPerformed

    private void btn_selecionarComprimentoOndaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_selecionarComprimentoOndaActionPerformed
        String comprimentoOnda = this.cmb_selecionarComprimentoOnda.getSelectedItem().toString();
        this.controller.getComprimentoOnda(comprimentoOnda);

        this.cmb_selecionarComprimentoOnda.setEnabled(false);
        this.btn_selecionarComprimentoOnda.setEnabled(false);
        this.cmb_selecionarDiametroNucleo.setEnabled(true);
        this.btn_selecionarDiametroNucleo.setEnabled(true);
        getRootPane().setDefaultButton(this.btn_selecionarDiametroNucleo);
    }//GEN-LAST:event_btn_selecionarComprimentoOndaActionPerformed

    private void cmb_selecionarDiametroNucleoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_selecionarDiametroNucleoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_selecionarDiametroNucleoActionPerformed

    private void btn_selecionarDiametroNucleoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_selecionarDiametroNucleoActionPerformed
        String diametroNucleo = this.cmb_selecionarDiametroNucleo.getSelectedItem().toString();
        this.controller.getDiametroNucleo(diametroNucleo);
        this.simulador = this.controller.createSimulador();

        this.cmb_selecionarDiametroNucleo.setEnabled(false);
        this.btn_selecionarDiametroNucleo.setEnabled(false);

        this.txt_distanciaDeformacao.setEditable(true);
        this.txt_variacaoNucleo.setEditable(true);
        this.txt_potenciaEntrada.setEditable(true);
        this.txt_comprimentoFibra.setEditable(true);
        this.txt_anguloIncidencia.setEditable(true);

        this.btn_simular.setEnabled(true);
    }//GEN-LAST:event_btn_selecionarDiametroNucleoActionPerformed

    private void txt_potenciaEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_potenciaEntradaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_potenciaEntradaActionPerformed

    private void txt_anguloIncidenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_anguloIncidenciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_anguloIncidenciaActionPerformed

    private void txt_comprimentoFibraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_comprimentoFibraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_comprimentoFibraActionPerformed

    private void txt_distanciaDeformacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_distanciaDeformacaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_distanciaDeformacaoActionPerformed

    private void txt_variacaoNucleoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_variacaoNucleoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_variacaoNucleoActionPerformed

    private void btn_simularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simularActionPerformed
        try {
            float potenciaEntrada = 0;
            float anguloIncidencia = 0;
            float comprimentoFibra = 0;
            float distanciaSemDeformacao = 0;
            float variacaoDiametroNucleo = 0;
            if (!(this.txt_anguloIncidencia.getText().trim().isEmpty()
                                || this.txt_comprimentoFibra.getText().trim().isEmpty()
                                || this.txt_potenciaEntrada.getText().trim().isEmpty()
                                || this.txt_distanciaDeformacao.getText().trim().isEmpty()
                                || this.txt_variacaoNucleo.getText().trim().isEmpty())) {
                try {
                    potenciaEntrada = Float.parseFloat(this.txt_potenciaEntrada.getText());

                    anguloIncidencia = Float.parseFloat(this.txt_anguloIncidencia.getText());
                    comprimentoFibra = Float.parseFloat(this.txt_comprimentoFibra.getText());
                    distanciaSemDeformacao = Float.parseFloat(this.txt_distanciaDeformacao.getText());
                    variacaoDiametroNucleo = Float.parseFloat(this.txt_variacaoNucleo.getText());
                } catch (NumberFormatException ex) {
                    String str1 = java.util.ResourceBundle.getBundle("ui/Bundle").getString("valoresInvalidos");
                    JOptionPane.showMessageDialog(this, str1, this.str, JOptionPane.ERROR_MESSAGE);
                }
                if (this.controller.adicionarDadosSimulacao(
                                    potenciaEntrada, anguloIncidencia, comprimentoFibra,
                                    distanciaSemDeformacao, variacaoDiametroNucleo)) {
                    String sim = java.util.ResourceBundle.getBundle("ui/Bundle").getString("sim");
                    String nao = java.util.ResourceBundle.getBundle("ui/Bundle").getString("nao");
                    String opcoes[] = {sim, nao};
                    String str1 = java.util.ResourceBundle.getBundle("ui/Bundle").getString("confirmaDados");
                    int resposta = JOptionPane.showOptionDialog(
                                        this, str1, this.str, 0,
                                        JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]);
                    if (resposta == 0) {
                        new ApresentarDadosSimulacaoComDeformacao(framePai, true, this.controller, this.controller.getSimuladorComDeformação());
                        this.dispose();
                    }
                } else {
                    String str1 = java.util.ResourceBundle.getBundle("ui/Bundle").getString("variacao");
                    JOptionPane.showMessageDialog(this, str1,
                                        this.str, JOptionPane.ERROR_MESSAGE);
                }
            } else {
                String str1 = java.util.ResourceBundle.getBundle("ui/Bundle").getString("dados");
                JOptionPane.showMessageDialog(this, str1, this.str, JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(
                                framePai,
                                e.getMessage(),
                                this.str,
                                JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_simularActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_selecionarBainha;
    private javax.swing.JButton btn_selecionarComprimentoOnda;
    private javax.swing.JButton btn_selecionarDiametroNucleo;
    private javax.swing.JButton btn_selecionarNucleo;
    private javax.swing.JButton btn_simular;
    private javax.swing.JComboBox cmb_selecionarBainha;
    private javax.swing.JComboBox cmb_selecionarComprimentoOnda;
    private javax.swing.JComboBox cmb_selecionarDiametroNucleo;
    private javax.swing.JComboBox cmb_selecionarNucleo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lbl_distanciaDeformacao;
    private javax.swing.JLabel lbl_potenciaEntrada;
    private javax.swing.JTextField txt_anguloIncidencia;
    private javax.swing.JTextField txt_comprimentoFibra;
    private javax.swing.JTextField txt_distanciaDeformacao;
    private javax.swing.JTextField txt_potenciaEntrada;
    private javax.swing.JTextField txt_variacaoNucleo;
    // End of variables declaration//GEN-END:variables
}
