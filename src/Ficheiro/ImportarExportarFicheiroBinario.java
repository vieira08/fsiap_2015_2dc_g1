/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficheiro;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import model.GestorSimuladores;

/**
 *
 * @author G01
 */
public class ImportarExportarFicheiroBinario {

    /**
     * Constrói uma instância de ImportarExportarFicheiroBinario
     */
    public ImportarExportarFicheiroBinario() {

    }

    /**
     * Método que irá ler/importar os dados das simulações recebendo como
     * parâmetro o nome dado ao Ficheiro
     *
     * @param nomeFicheiro nome do ficheiro
     * @return a leitura do ficheiro lido
     */
    public GestorSimuladores ler(String nomeFicheiro) {
        GestorSimuladores gestorSimuladores;
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(nomeFicheiro));
            try {
                gestorSimuladores = (GestorSimuladores) in.readObject();
            } finally {
                in.close();
            }
            return gestorSimuladores;
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }

    /**
     * Método que irá guardar/exportar os dados das simulações recebendo como
     * parâmetro o nome dado ao Ficheiro e todas as simulações
     *
     * @param nomeFicheiro nome do ficheiro
     * @param gestor todas as simulações
     * @return true se conseguiu guardar os dados
     */
    public boolean guardar(String nomeFicheiro, GestorSimuladores gestorSimuladores) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(gestorSimuladores);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
