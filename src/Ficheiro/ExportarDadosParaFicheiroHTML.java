/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ficheiro;

import java.util.ArrayList;
import model.GestorSimuladores;
import model.Simulador;

/**
 *
 * @author G01
 */
public class ExportarDadosParaFicheiroHTML {

    /**
     * Gestor de Simuladores.
     */
    GestorSimuladores gestor;

    /**
     * Cria uma instância da classe.
     */
    public ExportarDadosParaFicheiroHTML() {
        this.gestor = GestorSimuladores.getInstance();
    }

    /**
     * Devolve a representação em html dos dados do simulador recebido como parâmetro.
     * @param simulador simulador
     * @return representação html
     */
    public String getDadosSimulador(Simulador simulador) {
        String aux = "%n<center><table>";
        aux += "%n<h5>";
        aux += "%n<p>Ângulo Máximo de Aceitação: " + simulador.calcularAnguloMaximoAceitacao() + "</p>";
        aux += "%n<p> Cone de Aceitação: " + simulador.calcularConeAceitacao() + "</p>";
        aux += "%n<p> Abertura Numérica: " + simulador.calcularAberturaNumerica() + "</p>";
        aux += "%n<p> Potência de Saída: " + simulador.calcularPotenciaSaida() + "</p>";
        aux += "%n<p> Ângulo Crítico: " + simulador.calcularAnguloCritico() + "</p>";
        aux += "%n<p> Frequência Normalizada: " + simulador.calcularFrequenciaNormalizada() + "</p>";
        aux += "%n<p> Potência de Entrada: " + simulador.getPotenciaEntrada() + "</p>";
        aux += "%n<p> Comprimento da Fibra: " + simulador.getComprimentoFibra() + "</p>";
        aux += "%n<p> Ângulo de Incidência: " + simulador.getAnguloIncidencia() + "</p>";
        aux += "%n<p> Comprimento de Onda: " + simulador.getComprimentoOnda() + "</p>";
        aux += "%n<p> Diâmetro do Núcleo: " + simulador.getDiametroNucleo() + "</p>";
        aux += "%n</h5>";
        aux += "%n</table></center>";
        return aux;
    }

    /**
     * Devolve a representação em html para o abrir.
     * @param nomeFich nome do ficheiro html
     * @return representação html
     */
    public String abrirHTML(String nomeFich) {
        return ("<!DOCTYPE html>"
                + "<html>%n\t<head>%n\t\t<title>" + nomeFich + "</title>"
                + "%n\t\t<meta charset=\"utf-8\">"
                + "%n\t</head>%n\t<body>");
    }

    /**
     * Devolve a representação em html para a inserção de uma imagem.
     * @return representação html
     */
    public String imagemHTML() {
        String aux = "<center><figure>";
        aux += "<img src=http://www.dei.isep.ipp.pt/images/topo_index.png>";
        aux += "</figure></center>";
        return aux;
    }

    /**
     * Devolve a representação em html para a inserção de um cabeçalho.
     * @param s titulo do cabeçalho
     * @param tamanho tamanho do cabeçalho
     * @return representação html
     */
    public String cabecalhos(String s, int tamanho) {
        return ("<center><h" + tamanho + "><font color=\"#8A0808\">" + s + "</font></h" + tamanho + "></center>");
    }

    /**
     * Devolve a representação em html para o fechar.
     * @return representação html
     */
    public String fecharHTML() {
        return ("%n\t</body>%n</html>");
    }
}
