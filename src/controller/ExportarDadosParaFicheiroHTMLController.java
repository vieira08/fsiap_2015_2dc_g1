package controller;

import Ficheiro.ExportarDadosParaFicheiroHTML;
import java.io.File;
import java.io.IOException;
import java.util.Formatter;
import javax.swing.JOptionPane;
import model.GestorSimuladores;
import model.Simulador;

/**
 *
 * @author G01
 */
public class ExportarDadosParaFicheiroHTMLController {

    /**
     * Instância de ExportarDadosParaFicheiroHTML.
     */
    ExportarDadosParaFicheiroHTML exportarDados;
    
    /**
     * Simulador.
     */
    Simulador simulador;
    
    /**
     * Gestor de simuladores.
     */
    GestorSimuladores gestor;

    /**
     * Nome do ficheiro html.
     */
    public static final String FILE_NAME = "dados.html";

    /**
     * Cria uma instância desta classe.
     */
    public ExportarDadosParaFicheiroHTMLController() {
        this.exportarDados = new ExportarDadosParaFicheiroHTML();
        this.gestor = GestorSimuladores.getInstance();
    }

    /**
     * Constrói o ficheiro html de acordo com os métodos recebido da classe ExportarDadosParaFicheiroHTML.
     */
    public void guardar() {
        try {
            File file = new File(FILE_NAME);
            Formatter fOut = new Formatter(file, "UTF-8");
            if (file.exists()) {
                fOut.format(exportarDados.abrirHTML(FILE_NAME));

                fOut.format(exportarDados.imagemHTML());

                int i = 1;
                for (Simulador s : gestor.getListaSimuladores()) {
                    fOut.format(exportarDados.cabecalhos(i + "ª Simulação\n\n\n", 3));
                    i++;
                    fOut.format(exportarDados.getDadosSimulador(s));
                }
                
                fOut.format(exportarDados.fecharHTML());
                fOut.close();
            } else {
                JOptionPane.showMessageDialog(null, "O ficheiro não existe!", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
