/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.Set;
import model.FibraOtica;
import model.RegistoDados;
import model.Simulador;
import model.SimuladorComDeformacao;

/**
 *
 * @author G01
 */
public class SimularTransmissaoComDeformacaoController {

    /**
     * Instancia de Registo de Dados.
     */
    private RegistoDados registoDados;

    /**
     * Material de Nucleo.
     */
    private String materialNucleo;

    /**
     * Indice de Nucleo.
     */
    private float indiceNucleo;

    /**
     * Material da Bainha.
     */
    private String materialBainha;

    /**
     * Indice da Bainha.
     */
    private float indiceBainha;

    /**
     * Instancia de fibra otica.
     */
    private FibraOtica fibraOtica;

    /**
     * Comprimento de Onda.
     */
    private float comprimentoDeOnda;

    /**
     * Diametro do Nucleo.
     */
    private float diametroDoNucleo;

    /**
     * Instancia de Simulador com deformação.
     */
    private SimuladorComDeformacao simuladorComDeformacao;
    /**
     * Distância da fibra sem deformação.
     */
    private float distanciaSemDeformacao;
    /**
     * Variação do diâmetro do núcleo.
     */
    private float variacaoDiametroNucleo;
    /**
     * Comprimento da fibra.
     */
    private float comprimentoFibra;

    /**
     * Constrói uma instância de SimularComDeformacaoController
     */
    public SimularTransmissaoComDeformacaoController() {
        this.registoDados = RegistoDados.getInstance();
        this.materialNucleo = null;
        this.indiceNucleo = 0;
        this.materialBainha = null;
        this.indiceBainha = 0;
        this.fibraOtica = null;
        this.comprimentoDeOnda = 0;
        this.diametroDoNucleo = 0;
        this.simuladorComDeformacao = null;
    }

    /**
     * Devolve o registo de dados
     *
     * @return Registo de Dados
     */
    public RegistoDados getRegistoDados() {
        this.registoDados = RegistoDados.getInstance();
        this.registoDados.preencherListas();
        return this.registoDados;
    }

    /**
     * Devolve a lista de materiais do nucleo
     *
     * @return Lista de materiais do nucleo
     */
    public Set<String> getListaMateriaisNucleo() {
        return this.registoDados.getListaMateriaisNucleo();
    }

    /**
     * Recebe do Registo de Dados o indice de refracao associado ao material
     * recebido como parametro
     *
     * @param materialNucleo Material do Nucleo
     */
    public void getMaterialNucleo(String materialNucleo) {
        this.materialNucleo = materialNucleo;
        this.indiceNucleo = this.registoDados.getIndiceNucleo(materialNucleo);
    }

    /**
     * Devolve a lista de materiais da bainha
     *
     * @return Lista d de materiais da bainha
     */
    public List<String> getListaMateriaisBainha() {
        return this.registoDados.getListaMateriaisBainha(indiceNucleo);
    }

    /**
     * Recebe do Registo de Dados o indice de refracao associado ao material
     * recebido como parametro
     *
     * @param materialBainha Material da Bainha
     */
    public void getMaterialBainha(String materialBainha) {
        this.materialBainha = materialBainha;
        this.indiceBainha = this.registoDados.getIndiceBainha(materialBainha);
    }

    /**
     * Cria uma fibra otica
     */
    public void createFO() {
        this.fibraOtica = new FibraOtica(materialNucleo, indiceNucleo,
                            materialBainha, indiceBainha);
    }

    /**
     * Devolve a lista de comprimentos de onda
     *
     * @return Lista de comprimentos de onda
     */
    public Set<String> getListaComprimentoOnda() {
        return this.registoDados.getListaComprimentoOnda();
    }

    /**
     * Recebe do Registo de Dados o comprimento de onda associado à String
     * recebida como parametro
     *
     * @param comprimentoOnda Comprimento de Onda
     */
    public void getComprimentoOnda(String comprimentoOnda) {
        this.comprimentoDeOnda = this.registoDados.getComprimentoOnda(comprimentoOnda);
    }

    /**
     * Devolve a lista de diametros do nucleo
     *
     * @return Lista de diametros do nucleo
     */
    public Set<String> getListaDiametroNucleo() {
        return this.registoDados.getListaDiametroNucleo();
    }

    /**
     * Recebe do Registo de Dados o diametro do nucleo associado à String
     * recebida como parametro
     *
     * @param diametroNucleo Diametro do Nucleo
     */
    public void getDiametroNucleo(String diametroNucleo) {
        this.diametroDoNucleo = this.registoDados.getDiametroNucleo(diametroNucleo);
    }

    /**
     * Cria uma simulacao.
     */
    public SimuladorComDeformacao createSimulador() {
        this.simuladorComDeformacao = new SimuladorComDeformacao(fibraOtica,
                            diametroDoNucleo, indiceBainha, distanciaSemDeformacao, comprimentoFibra);
        return this.simuladorComDeformacao;
    }

    /**
     * Modifica a potencia de entrada e o angulo de incidencia
     *
     * @param potenciaEntrada Potencia de Entrada
     * @param anguloIncidencia Angulo de Incidencia
     * @return true se os dados nao sao null nem igual a 0 e false caso
     * contrario
     */
    public boolean adicionarDadosSimulacao(float potenciaEntrada, float anguloIncidencia,
                        float comprimentoFibra, float distanciaSemDeformacao, float variacaoDiametroNucleo) {
        this.simuladorComDeformacao.setPotenciaEntrada(potenciaEntrada);
        this.simuladorComDeformacao.setAnguloIncidencia(anguloIncidencia);
        this.simuladorComDeformacao.setComprimentoFibra(comprimentoFibra);
        boolean alteradaDistancia = this.simuladorComDeformacao.setDistanciaSemDeformacao(distanciaSemDeformacao);
        boolean alteradaVariacao = this.simuladorComDeformacao.setVariacaoDiametroNucleo(variacaoDiametroNucleo);
        boolean criadoSimulador = this.simuladorComDeformacao.criarSimuladorComDeformacao();
        return alteradaDistancia && alteradaVariacao && criadoSimulador;
    }

    /**
     * Devolve o Simulador com deformação
     *
     * @return Simulador com Deformação
     */
    public Simulador getSimuladorComDeformação() {
        return this.simuladorComDeformacao.getSimuladorComDeformacao();
    }

    /**
     * Devolve o angulo maximo de aceitacao
     *
     * @return Angulo Maximo de Acetacao
     */
    public double calcularAnguloMaximoAceitacao() {
        return this.simuladorComDeformacao.calcularAnguloMaximoAceitacao();
    }

    /**
     * Devolve o cone de aceitacao
     *
     * @return Cone de Acetacao
     */
    public double calcularConeAceitacao() {
        return this.simuladorComDeformacao.calcularConeAceitacao();
    }

    /**
     * Calcula a abertura numerica
     *
     * @return Abertura Numerica
     */
    public double calcularAberturaNumerica() {
        return this.simuladorComDeformacao.calcularAberturaNumerica();
    }

    /**
     * Calcula o angulo critico
     *
     * @return Angulo Critico
     */
    public double calcularAnguloCritico() {
        return this.simuladorComDeformacao.calcularAnguloCritico();
    }

    /**
     * Calcula a potencia de saida
     *
     * @return Potencia de Saida
     */
    public double calcularPotenciaSaida() {
        return this.simuladorComDeformacao.calcularPotenciaSaida();
    }

    /**
     * Calcula a frequencia normalizada
     *
     * @return Frequencia Normalizada
     */
    public double calcularFrequenciaNormalizada() {
        return this.simuladorComDeformacao.calcularFrequenciaNormalizada();
    }

    /**
     * Classifica a fibra em monomodo ou multimodo
     *
     * @return String com classificacao da fibra
     */
    public String classificaFibra() {
        return this.simuladorComDeformacao.classificaFibra();
    }
}
