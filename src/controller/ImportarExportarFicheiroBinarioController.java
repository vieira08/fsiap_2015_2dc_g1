/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Ficheiro.ImportarExportarFicheiroBinario;
import model.GestorSimuladores;

/**
 *
 * @author G01
 */
public class ImportarExportarFicheiroBinarioController {
/**
 * Instância de ImportarExportarFicheiroBinario
 */
    private ImportarExportarFicheiroBinario ficheiro = new ImportarExportarFicheiroBinario();
/**
 * Constrói uma instância de ImportarExportarFicheiroBinarioController
 */
    public ImportarExportarFicheiroBinarioController() {
        this.ficheiro = new ImportarExportarFicheiroBinario();
    }
/**
 * Método que irá ler/importar os dados das simulações recebendo como parâmetro o nome dado ao Ficheiro
 * @param nomeFicheiro nome do ficheiro
 * @return a leitura do ficheiro lido
 */
    public GestorSimuladores importarDadosParaFicheiroBinario(String nomeFicheiro) {
        return ficheiro.ler(nomeFicheiro);
    }
    /**
     * Método que irá guardar/exportar os dados das simulações recebendo como
     * parâmetro o nome dado ao Ficheiro e todas as simulações
     *
     * @param nomeFicheiro nome do ficheiro
     * @param gestor todas as simulações
     * @return true se conseguiu guardar os dados
     */
    public boolean exportarDadosParaFicheiroBinario(String nomeFicheiro, GestorSimuladores gestor) {
        return ficheiro.guardar(nomeFicheiro, gestor);
    }
}
