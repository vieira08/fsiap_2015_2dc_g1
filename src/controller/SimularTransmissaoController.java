/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.Set;
import model.FibraOtica;
import model.RegistoDados;
import model.Simulador;

/**
 *
 * @author G01
 */
public class SimularTransmissaoController {

    /**
     * Instancia de Registo de Dados
     */
    private RegistoDados registoDados;

    /**
     * Material de Nucleo
     */
    private String materialNucleo;

    /**
     * Indice de Nucleo
     */
    private float indiceNucleo;

    /**
     * Material da Bainha
     */
    private String materialBainha;

    /**
     * Indice da Bainha
     */
    private float indiceBainha;

    /**
     * Instancia de fibra otica
     */
    private FibraOtica fibraOtica;

    /**
     * Comprimento de Onda
     */
    private float comprimentoDeOnda;

    /**
     * Diametro do Nucleo
     */
    private float diametroDoNucleo;

    /**
     * Instancia de Simulador
     */
    private Simulador simulador;

    /**
     * Constrói uma instância de SimularTransmissaoController
     */
    public SimularTransmissaoController() {
        this.registoDados = RegistoDados.getInstance();
        this.materialNucleo = null;
        this.indiceNucleo = 0;
        this.materialBainha = null;
        this.indiceBainha = 0;
        this.fibraOtica = null;
        this.comprimentoDeOnda = 0;
        this.diametroDoNucleo = 0;
        this.simulador = null;
    }

    /**
     * Devolve o registo de dados
     *
     * @return Registo de Dados
     */
    public RegistoDados getRegistoDados() {
        this.registoDados = RegistoDados.getInstance();
        this.registoDados.preencherListas();
        return this.registoDados;
    }

    /**
     * Devolve a lista de materiais do nucleo
     *
     * @return Lista de materiais do nucleo
     */
    public Set<String> getListaMateriaisNucleo() {
        return this.registoDados.getListaMateriaisNucleo();
    }

    /**
     * Recebe do Registo de Dados o indice de refracao associado ao material
     * recebido como parametro
     *
     * @param materialNucleo Material do Nucleo
     */
    public void getMaterialNucleo(String materialNucleo) {
        this.materialNucleo = materialNucleo;
        this.indiceNucleo = this.registoDados.getIndiceNucleo(materialNucleo);
    }

    /**
     * Devolve a lista de materiais da bainha
     *
     * @return Lista d de materiais da bainha
     */
    public List<String> getMateriaisBainha() {
        return this.registoDados.getListaMateriaisBainha(indiceNucleo);
    }

    /**
     * Recebe do Registo de Dados o indice de refracao associado ao material
     * recebido como parametro
     *
     * @param materialBainha Material da Bainha
     */
    public void getMaterialBainha(String materialBainha) {
        this.materialBainha = materialBainha;
        this.indiceBainha = this.registoDados.getIndiceBainha(materialBainha);
    }

    /**
     * Cria uma fibra otica
     */
    public void createFO() {
        this.fibraOtica = new FibraOtica(materialNucleo, indiceNucleo,
                            materialBainha, indiceBainha);
    }

    /**
     * Devolve a lista de comprimentos de onda
     *
     * @return Lista de comprimentos de onda
     */
    public Set<String> getComprimentoOnda() {
        return this.registoDados.getListaComprimentoOnda();
    }

    /**
     * Recebe do Registo de Dados o comprimento de onda associado à String
     * recebida como parametro
     *
     * @param comprimentoOnda Comprimento de Onda
     */
    public void getComprimentoOnda(String comprimentoOnda) {
        this.comprimentoDeOnda = this.registoDados.getComprimentoOnda(comprimentoOnda);
    }

    /**
     * Devolve a lista de diametros do nucleo
     *
     * @return Lista de diametros do nucleo
     */
    public Set<String> getListaDiametroNucleo() {
        return this.registoDados.getListaDiametroNucleo();
    }

    /**
     * Recebe do Registo de Dados o diametro do nucleo associado à String
     * recebida como parametro
     *
     * @param diametroNucleo Diametro do Nucleo
     */
    public void getDiametroNucleo(String diametroNucleo) {
        this.diametroDoNucleo = this.registoDados.getDiametroNucleo(diametroNucleo);
    }

    /**
     * Cria uma simulacao.
     */
    public Simulador createSimulador() {
        this.simulador = new Simulador(fibraOtica, diametroDoNucleo, comprimentoDeOnda);
        return this.simulador;
    }

    /**
     * Modifica a potencia de entrada e o angulo de incidencia
     *
     * @param potenciaEntrada Potencia de Entrada
     * @param anguloIncidencia Angulo de Incidencia
     * @return true se os dados nao sao null nem igual a 0 e false caso
     * contrario
     */
    public boolean adicionarDadosSimulacao(float potenciaEntrada, float anguloIncidencia, float comprimentoFibra) {
        this.simulador.setPotenciaEntrada(potenciaEntrada);
        this.simulador.setAnguloIncidencia(anguloIncidencia);
        this.simulador.setComprimentoFibra(comprimentoFibra);
        return this.simulador.validaDados();
    }

    /**
     * Devolve o angulo maximo de aceitacao
     *
     * @return Angulo Maximo de Acetacao
     */
    public double calcularAnguloMaximoAceitacao() {
        return this.simulador.calcularAnguloMaximoAceitacao();
    }

    /**
     * Devolve o cone de aceitacao
     *
     * @return Cone de Acetacao
     */
    public double calcularConeAceitacao() {
        return this.simulador.calcularConeAceitacao();
    }

    /**
     * Calcula a abertura numerica
     *
     * @return Abertura Numerica
     */
    public double calcularAberturaNumerica() {
        return this.simulador.calcularAberturaNumerica();
    }

    /**
     * Calcula o angulo critico
     *
     * @return Angulo Critico
     */
    public double calcularAnguloCritico() {
        return this.simulador.calcularAnguloCritico();
    }

    /**
     * Calcula a potencia de saida
     *
     * @return Potencia de Saida
     */
    public double calcularPotenciaSaida() {
        return this.simulador.calcularPotenciaSaida();
    }

    /**
     * Calcula a frequencia normalizada
     *
     * @return Frequencia Normalizada
     */
    public double calcularFrequenciaNormalizada() {
        return this.simulador.calcularFrequenciaNormalizada();
    }

    /**
     * Classifica a fibra em monomodo ou multimodo
     *
     * @return String com classificacao da fibra
     */
    public String classificaFibra() {
        return this.simulador.classificaFibra();
    }

}
