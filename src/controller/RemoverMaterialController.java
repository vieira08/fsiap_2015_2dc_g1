/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.Set;
import javax.swing.DefaultListModel;
import model.RegistoDados;
import sun.awt.DefaultMouseInfoPeer;

/**
 *
 * @author G01
 */
public class RemoverMaterialController {

    /**
     * Instancia de material.
     */
    private RegistoDados re;

    /**
     * Constrói uma instância de RemoverMaterial.
     */
    public RemoverMaterialController() {
        this.re = RegistoDados.getInstance();
    }

    /**
     * Lista de materiais disponiveis.
     *
     * @return lista de materiais
     */
    public Set<String> listaMateriais() {
        return re.getListaMateriaisNucleo();
    }

    /**
     * Remove material recebido por parâmetro.
     *
     * @return verdadeiro caso seja removido e falso caso não.
     */
    public boolean removerMaterial(String material) {
        return this.re.getMaterial().removerMaterial(material);
    }
}
