/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.RegistoDados;

/**
 *
 * @author G01
 */
public class RegistarNovoMaterialController {
    
    /**
     * Instancia de material.
     */
   private RegistoDados re;
     
    /**
     * Constrói uma instância de RegistarNovoMaterialController.
     */
    public RegistarNovoMaterialController(){
        this.re = RegistoDados.getInstance();
    }
    
/**
     * Cria um novo material.
     * @param nomeMaterial
     * @param indiceRefracao
     * @return true caso seja criado.
     */
    public boolean adicionarMaterial(String nomeMaterial, float indiceRefracao){
        return re.getMaterial().adicionaMaterial(nomeMaterial, indiceRefracao);
    }
}