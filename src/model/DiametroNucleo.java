/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author G01
 */
public class DiametroNucleo implements Serializable{

    /**
     * Lista de Diâmetros do núcleo disponíveis.
     */
    private HashMap<String, Float> listaDiametroNucleo = new HashMap<String, Float>();

    /**
     * Constrói a instância acima referida.
     */
    public DiametroNucleo() {
        this.listaDiametroNucleo = new HashMap<>();
    }

    /**
     * Devolve todas as identificações dos diferentes diâmetros que se encontram
     * disponíveis.
     *
     * @return lista das identificações dos diferentes diâmetros disponíveis
     */
    public Set<String> getListaDiametroNucleo() {
        return listaDiametroNucleo.keySet();
    }

    /**
     * Devolve o valor do diâmetro a associado à identificação (chave), recebido
     * por parâmetro
     *
     * @param diametro identificação do diâmetro na lista
     *
     * @return valor do diâmetros associado ao nome
     */
    public float getDiametroNucleo(String diametro) throws NullPointerException{
        return this.listaDiametroNucleo.get(diametro);

    }

    /**
     * Preenche a lista de diâmetros do núcleo (HashMap).
     */
    public void preencheListaDiametroNucleo() {

        this.listaDiametroNucleo.put("5",   0.000005f);
        this.listaDiametroNucleo.put("6",   0.000006f);
        this.listaDiametroNucleo.put("7",   0.000007f);
        this.listaDiametroNucleo.put("8",   0.000008f);
        this.listaDiametroNucleo.put("9",   0.000009f);
        this.listaDiametroNucleo.put("10",  0.000010f);
    }

}
