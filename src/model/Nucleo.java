package model;

import java.io.Serializable;

/**
 * Classe que representa o núcleo de uma fibra ótica.
 * @author G01
 */
public class Nucleo implements Serializable{
   
    /**
     * Nome do material do núcleo.
     */
    private String nomeMaterial;
    
    /**
     * Indíce do material do núcleo.
     */
    private float indiceMaterial;
    
    /**
     * Construtor que cria uma instância de núcleo com o nome e o índice recebidos como parâmetro.
     * @param nomeMaterial nome do material
     * @param indiceMaterial índice do material
     */
    public Nucleo(String nomeMaterial, float indiceMaterial){
        this.nomeMaterial = nomeMaterial;
        this.indiceMaterial = indiceMaterial;
    }
    
    /**
     * Construtor vazio.
     */
    public Nucleo(){
        this.nomeMaterial = null;
        this.indiceMaterial = 0f;
    }
    
    /**
     * Construtor que cria uma instânca de núcleo através de outro núcleo recebido como parâmetro.
     * @param outroNucleo outro núcleo
     */
    public Nucleo(Nucleo outroNucleo){
       this.nomeMaterial = outroNucleo.getNomeMaterial();
       this.indiceMaterial = outroNucleo.getIndiceMaterial();
    }

    /**
     * Devolve o nome do material do núcleo.
     * @return nome do material
     */
    public String getNomeMaterial() {
        return nomeMaterial;
    }

    /**
     * Modifica o nome do material do núcleo.
     * @param nomeMaterial nome do material
     */
    public void setNomeMaterial(String nomeMaterial) {
        this.nomeMaterial = nomeMaterial;
    }

    /**
     * Devolve o índice do material do núcleo.
     * @return índice do material
     */
    public float getIndiceMaterial() {
        return indiceMaterial;
    }

    /**
     * Modifica o índice do material do núcleo.
     * @param indiceMaterial indice do material
     */
    public void setIndiceMaterial(float indiceMaterial) {
        this.indiceMaterial = indiceMaterial;
    }

    /**
     * Devolve uma representação textual do nome e do índice do núcleo.
     * @return representação textual
     */
    @Override
    public String toString() {
        return "Nome do núcleo: " + this.nomeMaterial + "\n Índice do núcleo: " + this.indiceMaterial;
    }
    
    /**
     * Compara dois nucleos diferentes.
     * @param obj outro nucleo
     * @return true se forem iguais; false se forem diferentes
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Nucleo outroNucleo = (Nucleo) obj;
        return (this.nomeMaterial.equalsIgnoreCase(outroNucleo.nomeMaterial) && this.indiceMaterial == outroNucleo.indiceMaterial);
    }
}
