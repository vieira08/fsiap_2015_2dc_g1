/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

/**
 *
 * @author G01
 */
public class RegistoDados implements Serializable {

    private DiametroNucleo diametroNucleo;
    private ComprimentoOnda comprimentoOnda;
    private Material material;

    private static final RegistoDados registoDados = new RegistoDados();

    private RegistoDados() {
        this.comprimentoOnda = new ComprimentoOnda();
        this.diametroNucleo = new DiametroNucleo();
        this.material = new Material();
    }

    public Material getMaterial() {
        return this.material;
    }

    /**
     * Devolve a lista de materiais disponíveis para o núcleo
     *
     * @return lista dos nomes dos materiais disponíveis para o núcleo
     */
    public Set<String> getListaMateriaisNucleo() {
        return this.material.getListaMateriaisNucleo();
    }

    /**
     * Devolve a lista de materiais disponíveis para a bainha
     *
     * @return lista dos nomes dos materiais disponíveis para a bainha
     */
    public ArrayList<String> getListaMateriaisBainha(float indice) {
        return this.material.getListaMateriaisBainha(indice);
    }

    /**
     * Devolve a lista dos tipos de onda disponíveis para selecção
     *
     * @return lista dos tipos de onda disponíveis
     */
    public Set<String> getListaComprimentoOnda() {
        return this.comprimentoOnda.getListaComprimentoOnda();
    }

    /**
     * Devolve a lista de identificadores dos diâmetros do núcleo
     *
     * @return lista dos identificadores dos diâmetros do núcleo
     */
    public Set<String> getListaDiametroNucleo() {
        return this.diametroNucleo.getListaDiametroNucleo();
    }

    /**
     * Devolve o valor do comprimento de onda associado ao nome do mesmo
     * (chave), recebido por parâmetro
     *
     * @param comprimentoOnda nome do tipo de onda asssociado a determinado
     * comprimento
     * @return valor do comprimento de onda
     */
    public float getComprimentoOnda(String comprimentoOnda) {
        return this.comprimentoOnda.getComprimentoOnda(comprimentoOnda);
    }

    /**
     * Devolve o valor do diâmetro a associado à identificação (chave), recebido
     * por parâmetro
     *
     * @param diametro identificação do diâmetro na lista
     *
     * @return valor do diâmetros associado ao nome
     */
    public float getDiametroNucleo(String diametro) {
        return this.diametroNucleo.getDiametroNucleo(diametro);
    }

    /**
     * Devolve o indice do material escolhido para o núcleo
     *
     * @param material material escolhido para o núcleo
     * @return indice do material escolhido para o núcleo
     */
    public float getIndiceNucleo(String material) {
        return this.material.getIndice(material);
    }

    /**
     * Devolve o indice do material escolhido para a bainha
     *
     * @param material material escolhido para a bainha
     * @return indice do material escolhido para a bainha
     */
    public float getIndiceBainha(String material) {
        return this.material.getIndice(material);
    }

    public static RegistoDados getInstance() {
        return registoDados;
    }

    public void preencherListas() {
        this.comprimentoOnda.preencheListaComprimentoOnda();
        this.diametroNucleo.preencheListaDiametroNucleo();
        this.material.preencheListaMateriais();
    }
}
