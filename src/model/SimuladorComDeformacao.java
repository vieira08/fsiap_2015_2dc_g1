/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.DecimalFormat;

/**
 *
 * @author G01
 */
public class SimuladorComDeformacao {

    /**
     * Simulador sem deformação.
     */
    private Simulador simuladorSemDeformacao;
    /**
     * Simulador com deformação.
     */
    private Simulador simuladorComDeformacao;

    /**
     * Variação do diâmetro do núcleo com a deformação.
     */
    private float variacaoDiametroNucleo;

    /**
     * Distância da fibra sem deformação.
     */
    private float distanciaSemDeformacao;

    /**
     * Comprimento total da fibra ótica.
     */
    private float comprimentoFibra;

    /**
     * Constrói uma instância de simulador com deformação recebendo como
     * parâmetro Fibra Ótica, o comprimento de onda e o diametro do nucleo.
     *
     * @param fo fibra ótica em analise.
     * @param diametroN diametro do nucleo da fibra ótica em analise.
     * @param compOnda comprimento de onda utilizado.
     * @param distanciaSemDeformacao distancia da fibra ótica sem deformacao do
     * diâmetro do núcleo.
     * @param variacaoDiametroNucleo variação do diâmetro do núcleo.
     */
    public SimuladorComDeformacao(FibraOtica fo, float diametroN, float compOnda, float distanciaSemDeformacao, float comprFibra) {
        this.simuladorComDeformacao = new Simulador();
        this.simuladorComDeformacao.setComprimentoOnda(compOnda);
        this.simuladorSemDeformacao = new Simulador(fo, diametroN, compOnda);
        this.variacaoDiametroNucleo = 0;
        setComprimentoFibra(comprFibra);
        setDistanciaSemDeformacao(distanciaSemDeformacao);
        //double variacaoDiamNucleoConvertidoMicro = variacaoDiametroNucleo * Math.pow(10, -6);
        //setVariacaoDiametroNucleo((float) variacaoDiamNucleoConvertidoMicro);

    }

    /**
     * Devolve o Simulador com deformação
     *
     * @return Simulador com Deformação
     */
    public Simulador getSimuladorComDeformacao() {
        return simuladorComDeformacao;
    }

    /**
     * Devolve a variação do diâmetro do núcleo
     *
     * @return variação do diâmetro do núcleo.
     */
    public float getVariacaoDiametroNucleo() {
        return variacaoDiametroNucleo;
    }

    /**
     * Devolve a distância sem deformação.
     *
     * @return distância sem deformação
     */
    public float getDistanciaSemDeformacao() {
        return distanciaSemDeformacao;
    }

    /**
     * Devolve o comprimento da fibra ótica em análise.
     *
     * @return comprimento da fibra ótica.
     */
    public float getComprimentoFibra() {
        return comprimentoFibra;
    }

    /**
     * Devolve o ângulo de incidência introduzido.
     *
     * @return ângulo de incidência.
     */
    public float getAnguloIncidencia() {
        return this.simuladorSemDeformacao.getAnguloIncidencia();
    }

    /**
     * Devolve a potência de entrada introduzida
     *
     * @return potência de entrada
     */
    public float getPotenciaEntrada() {
        return this.simuladorSemDeformacao.getPotenciaEntrada();
    }

    /**
     * Modifica o ângulo de incidência na fibra ótica.
     *
     * @param anguloIncidencia ângulo de incidência
     */
    public void setAnguloIncidencia(float anguloIncidencia) {
        this.simuladorSemDeformacao.setAnguloIncidencia(anguloIncidencia);
        this.simuladorComDeformacao.setAnguloIncidencia(anguloIncidencia);
    }

    /**
     * Modifica o comprimento da Fibra Ótica em estudo.
     *
     * @param comprimentoFO novo comprimento da fibra ótica
     */
    public void setComprimentoFibra(float comprimentoFO) {
        if (comprimentoFO > 0) {
            this.comprimentoFibra = comprimentoFO;
        }

    }

    /**
     * Modifica a potência de entrada da fibra ótica.
     *
     * @param potenciaEntrada potência de entrada.
     */
    public void setPotenciaEntrada(float potenciaEntrada) {
        this.simuladorSemDeformacao.setPotenciaEntrada(potenciaEntrada);
    }

    /**
     * Modifica a distância sem deformação.
     *
     * @param distanciaSemDeformacao distância da fibra sem deformação
     * @return true caso seja válido e falso caso não seja
     */
    public boolean setDistanciaSemDeformacao(float distanciaSemDeformacao) {
        if (distanciaSemDeformacao > 0 && distanciaSemDeformacao < this.comprimentoFibra) {
            this.distanciaSemDeformacao = distanciaSemDeformacao;
            return true;
        }
        return false;
    }

    /**
     * Modifica a variação ocorrida no diâmetro do núcleo.
     *
     * @param variacaoDiametroNucleo variação do diâmetro do núcleo
     * @return true caso seja válido e falso caso não seja
     */
    public boolean setVariacaoDiametroNucleo(float variacaoDiametroNucleo) {
        double variacaoDiamNucleoConvertidoMicro = variacaoDiametroNucleo * Math.pow(10, -6);
        if (variacaoDiamNucleoConvertidoMicro > 0 && variacaoDiamNucleoConvertidoMicro < this.simuladorSemDeformacao.getDiametroNucleo()) {
            this.variacaoDiametroNucleo = (float) variacaoDiamNucleoConvertidoMicro;
            return true;
        }
        return false;
    }

    /**
     * Constrói um simulador com deformacao completo.
     */
    public boolean criarSimuladorComDeformacao() {

        if (preencherSimuladorSemDeformacao()) {
            iniciarSimulacaoSemDeformacao();
            return construirSimuladorComDeformacao();
        }
        return false;
    }

    /**
     * Calcula o angulo máximo de aceitacao
     *
     * @return angulo maximo de aceitacao
     */
    public double calcularAnguloMaximoAceitacao() {
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.simuladorComDeformacao.calcularAnguloMaximoAceitacao()).replace(",","."));
    }

    /**
     * Calcula o cone de aceitacao
     *
     * @return o valor do cone de aceitacao
     */
    public double calcularConeAceitacao() {
       DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.simuladorComDeformacao.calcularConeAceitacao()).replace(",","."));
    }

    /**
     * Calcula a abertura numérica associada a fibra em estudo
     *
     * @return o valor da abertura numerica
     */
    public double calcularAberturaNumerica() {
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.simuladorComDeformacao.calcularAberturaNumerica()).replace(",","."));
    }

    /**
     * Calcula a potencia de saida da fibra otica
     *
     * @return potencia de saida
     */
    public double calcularPotenciaSaida() {
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.simuladorComDeformacao.calcularPotenciaSaida()).replace(",","."));
    }

    /**
     * Preenche e valida o simulador sem deformação.
     *
     * @return verdadeiro se o simulador sem deformação se encontra devidamente
     * preenchido e falso caso não esteja
     */
    private boolean preencherSimuladorSemDeformacao() {
        setComprimentoFibraDeCadaSimulador();
        return this.simuladorSemDeformacao.validaDados();
    }

    /**
     * Calcula o angulo critico da fibra otica
     *
     * @return o angulo critico
     */
    public double calcularAnguloCritico() {
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.simuladorComDeformacao.calcularAnguloCritico()).replace(",","."));
    }

    /**
     * Calcula a frequência normalizada da fibra ótica em análise.
     *
     * @return frequência normalizada
     */
    public double calcularFrequenciaNormalizada() {
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.simuladorComDeformacao.calcularFrequenciaNormalizada()).replace(",","."));
    }

    /**
     * Classifica a fibra consoante o seu valor da frequência normalizada.
     *
     * @return classificação da fibra ótica, sendo esta classificada entre
     * monomodo ou multimodo
     */
    public String classificaFibra() {
        return this.simuladorComDeformacao.classificaFibra();
    }

    /**
     * Inicia a simulação do simmulador sem deformação.
     */
    private void iniciarSimulacaoSemDeformacao() {
        this.simuladorSemDeformacao.calcularPotenciaSaida();
    }

    /**
     * Modifica o comprimento da Fibra Ótica dos simuladores.
     */
    private void setComprimentoFibraDeCadaSimulador() {
        this.simuladorComDeformacao.setComprimentoFibra(this.comprimentoFibra - this.distanciaSemDeformacao);
        this.simuladorSemDeformacao.setComprimentoFibra(this.distanciaSemDeformacao);
    }

    /**
     * Constrói um simulador com deformação completo.
     *
     * @return verdadeiro se for constuído com valores válidos e falso caso não
     * tenha sido.
     */
    private boolean construirSimuladorComDeformacao() {
        this.simuladorComDeformacao.setFo(this.simuladorSemDeformacao.getFo());
        float diametroNucleo = this.simuladorSemDeformacao.getDiametroNucleo() - this.variacaoDiametroNucleo;
        this.simuladorComDeformacao.setDiametroNucleo(diametroNucleo);
        float potenciaEntrada = (float) this.simuladorSemDeformacao.getPotenciaSaida();
        this.simuladorComDeformacao.setPotenciaEntrada(potenciaEntrada);
        return this.simuladorComDeformacao.validaDados();
    }

}
