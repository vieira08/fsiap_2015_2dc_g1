/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author G01
 */
public class ComprimentoOnda implements Serializable{

    /**
     * Lista de comprimentos de onda disponíveis.
     */
    private HashMap<String, Float> listaComprimentoOnda = new HashMap<String, Float>();

    /**
     * Construtor instância acima referida.
     */
    public ComprimentoOnda() {
        this.listaComprimentoOnda = new HashMap<>();
    }

    /**
     * Devolve todos os nomes dos diferentes tipos de onda associados a
     * determinados comprimentos de onda que se encontram disponíveis.
     *
     * @return lista dos nomes dos tipos de onda disponíveis
     */
    public Set<String> getListaComprimentoOnda() {
        return listaComprimentoOnda.keySet();
    }

    /**
     * Devolve o valor do comprimento de onda associado ao nome do mesmo
     * (chave), recebido por parâmetro
     *
     * @param comprimentoOnda nome do tipo de onda asssociado a determinado
     * comprimento
     * @return valor do comprimento de onda
     */
    public float getComprimentoOnda(String comprimentoOnda) throws NullPointerException {
        return this.listaComprimentoOnda.get(comprimentoOnda);

    }

    /**
     * Preenche a lista de comprimento de onda ao HashMap.
     */
    public void preencheListaComprimentoOnda() {

        this.listaComprimentoOnda.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("visivelVioleta"), 0.425f);
        this.listaComprimentoOnda.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("visivelAzul"), 0.485f);
        this.listaComprimentoOnda.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("visivelVerde"), 0.533f);
        this.listaComprimentoOnda.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("visivelAmarelo"), 0.580f);
        this.listaComprimentoOnda.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("visivelLaranja"), 0.6f);
        this.listaComprimentoOnda.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("visivelVermelho"), 0.660f);
     }

}
