/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author G01
 */
public class GestorSimuladores implements Serializable {

    /**
     * Lista de simuladores.
     */
    private ArrayList<Simulador> listaSimuladores;
    /**
     * Única instância do gestor de simuladores.
     */
    private static GestorSimuladores gestor = new GestorSimuladores();

    /**
     * Constrói instância de gestor de simuladores sem parâmetros.
     */
    private GestorSimuladores() {
        this.listaSimuladores = new ArrayList<>();
    }

    /**
     * Devolve a única instância do gestor de simuladores.
     *
     * @return gestor de simuladores.
     */
    public static GestorSimuladores getInstance() {
        return gestor;
    }

    /**
     * Devolve a lista de simuladores.
     *
     * @return lista de simuladores.
     */
    public ArrayList<Simulador> getListaSimuladores() {
        return listaSimuladores;
    }

    public int inserirSimuladores(GestorSimuladores gestor) {
        int contador = 0;
        for (Simulador sim : gestor.getListaSimuladores()) {
            this.listaSimuladores.add(sim);
            contador++;
        }
        return contador;
    }

}
