/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 *
 * @author G01
 */
public class Simulador implements Serializable {

    /**
     * Potência de entrada do feixe luminoso.
     */
    private float potenciaEntrada;
    /**
     * Ângulo de incidência na fibra ótica.
     */
    private float anguloIncidencia;

    /**
     * Comprimento da fibra ótica.
     */
    private float comprimentoFibra;

    /**
     * Fibra ótica a analisar.
     */
    private FibraOtica fo;

    /**
     * Comprimento de onda da fibra otica.
     */
    private float comprimentoOnda;

    /**
     * Diâmetro do núcleo da fibra otica.
     */
    private float diametroNucleo;

    /**
     * Ângulo máximo de aceitação.
     */
    private double anguloMaxAceitacao;
    /**
     * Frequência normalizada - quantidade de modos permitidos em função do
     * perfil do índice da fibra.
     */
    private double freqNormalizada;
    /**
     * Abertura numérica da fibra em análise.
     */
    private double aberturaNumerica;
    /**
     * Valor do cone de aceitação.
     */
    private double coneAceitacao;
    /**
     * Valor da potência de saida.
     */
    private double potenciaSaida;
    /**
     * Valor do ângulo crítico.
     */
    private double anguloCritico;
    /**
     * Classificação da fibra.
     */
    private String classificaFibra;

    /**
     * Constrói uma instância de Simulador sem parâmetros.
     */
    public Simulador() {

    }

    /**
     * Constrói uma instância de Simulador recebendo como parâmetro Fibra Ótica,
     * o comprimento de onda e o diametro do nucleo.
     *
     * @param fo fibra ótica em analise.
     * @param comprOnda comprimento de onda utilizado.
     * @param diametroN diametro do nucleo da fibra ótica em analise.
     */
    public Simulador(FibraOtica fo, float diametroN, float compOnda) {
        this.diametroNucleo = diametroN;
        this.comprimentoOnda = compOnda;
        this.fo = fo;
        this.anguloIncidencia = 0;
        this.potenciaEntrada = 0;
        this.comprimentoFibra = 0;
    }

    /**
     * Devolve a potencia de entrada.
     *
     * @return a potencia de entrada usada para a simulação
     */
    public float getPotenciaEntrada() {
        return potenciaEntrada;
    }

    /**
     * Devolve a fibra ótica utilizada para a simulação
     *
     * @return fibra ótica usada na simulação
     */
    public FibraOtica getFo() {
        return fo;
    }

    /**
     * Comprimento da fibra otica utilizada na simulação
     *
     * @return o comprimento da fibra ótica
     */
    public float getComprimentoFibra() {
        return comprimentoFibra;
    }

    /**
     * Devolve o ângulo de incidência do feixe ótico na entrada usado na
     * simulação
     *
     * @return ãngulo de incidência da fibra ótica
     */
    public float getAnguloIncidencia() {
        return anguloIncidencia;
    }

    /**
     * Devolve o comprimento de onda do feixe de entrada.
     *
     * @return comprimento de onda
     */
    public float getComprimentoOnda() {
        return comprimentoOnda;
    }

    /**
     * Devolve o diametro do nucleo da fibra otica.
     *
     * @return diametro do nucleo
     */
    public float getDiametroNucleo() {
        return diametroNucleo;
    }

    /**
     * Devolve o potência de saída do feixe ótica na fibra otica.
     *
     * @return potência de saída
     */
    public double getPotenciaSaida() {
        return this.potenciaSaida;
    }

    /**
     * Devolve a Abertura Numerica.
     *
     * @return Abertura Numerica
     */
    public double getAberturaNumerica() {
        return aberturaNumerica;
    }

    /**
     * Devolve o Angulo Critico
     *
     * @return Angulo Critico
     */
    public double getAnguloCritico() {
        return anguloCritico;
    }

    /**
     * Devolve o Angulo Maximo Aceitacao
     *
     * @return Angulo Maximo Aceitacao
     */
    public double getAnguloMaxAceitacao() {
        return anguloMaxAceitacao;
    }

    /**
     * Devolve a classificacao da fibra
     *
     * @return classificacao da fibra
     */
    public String getClassificaFibra() {
        return classificaFibra;
    }

    /**
     * Devolve o Cone Aceitacao
     *
     * @return Cone Aceitacao
     */
    public double getConeAceitacao() {
        return coneAceitacao;
    }

    /**
     * Devolve a Frequencia Normalizada
     *
     * @return Frequencia Normalizada
     */
    public double getFreqNormalizada() {
        return freqNormalizada;
    }

    /**
     * Modifica o diametro do nucleo da fibra otica.
     *
     * @param diametroNucleo diametro do nucleo
     */
    public void setDiametroNucleo(float diametroNucleo) {
        this.diametroNucleo = diametroNucleo;
    }

    /**
     * Modifica a fibra otica em analise
     *
     * @param fo nova fibra otica
     */
    public void setFo(FibraOtica fo) {
        this.fo = fo;
    }

    /**
     * Modifica o comprimento de onda do feixe´otico de entrada
     *
     * @param comprimentoOnda nova comprimento de onde
     */
    public void setComprimentoOnda(float comprimentoOnda) {
        this.comprimentoOnda = comprimentoOnda;
    }

    /**
     * Modifica o ângulo de incidência na fibra ótica.
     *
     * @param anguloIncidencia ângulo de incidência
     */
    public void setAnguloIncidencia(float anguloIncidencia) {
        if (anguloIncidencia > 0) {
            this.anguloIncidencia = anguloIncidencia;
        }
    }

    /**
     * Modifica a potência de entrada da fibra ótica.
     *
     * @param potenciaEntrada potência de entrada.
     */
    public void setPotenciaEntrada(float potenciaEntrada) {
        if (potenciaEntrada > 0) {
            this.potenciaEntrada = potenciaEntrada;
        }
    }

    /**
     * Modifica o comprimento da Fibra Ótica em estudo.
     *
     * @param comprimentoFO novo comprimento da fibra ótica
     */
    public void setComprimentoFibra(float comprimentoFO) {
        if (comprimentoFO > 0) {
            this.comprimentoFibra = comprimentoFO;
        }
    }

    /**
     * Valida se os dados estão todos inseridos
     *
     * @return verdadeiro se estiverem preenchidos e falso se não
     */
    public boolean validaDados() {
        return this.anguloIncidencia > 0 && this.potenciaEntrada > 0
                && this.fo != null && this.comprimentoFibra > 0
                && this.comprimentoOnda > 0 && this.diametroNucleo > 0;
    }

    /**
     * Calcula o angulo máximo de aceitacao
     *
     * @return angulo maximo de aceitacao
     */
    public double calcularAnguloMaximoAceitacao() {
        double indiceAr = 1.0f;
        double valor = (Math.sqrt(Math.pow(fo.getNucleo().getIndiceMaterial(), 2) - Math.pow(fo.getBainha().getIndiceMaterial(), 2))) / indiceAr;
        double valorRadianos = Math.asin(valor);
        this.anguloMaxAceitacao = Math.toDegrees(valorRadianos);
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.anguloMaxAceitacao).replace(",","."));
    }

    /**
     * Calcula o cone de aceitacao
     *
     * @return o valor do cone de aceitacao
     */
    public double calcularConeAceitacao() {
        this.coneAceitacao = this.anguloMaxAceitacao * 2;
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.coneAceitacao).replace(",","."));
    }

    /**
     * Calcula a abertura numérica associada a fibra em estudo
     *
     * @return o valor da abertura numerica
     */
    public double calcularAberturaNumerica() {
        this.aberturaNumerica = Math.sqrt(Math.pow(fo.getNucleo().getIndiceMaterial(), 2) - Math.pow(fo.getBainha().getIndiceMaterial(), 2));
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.aberturaNumerica).replace(",","."));
    }

    /**
     * Calcula a potencia de saida da fibra otica
     *
     * @return potencia de saida
     */
    public double calcularPotenciaSaida() {
        double compFibraKm=this.comprimentoFibra/1000;
        this.potenciaSaida = Math.pow(10, (-calcularCoeficienteAtenuacao() * compFibraKm) / 10) * this.potenciaEntrada;
        DecimalFormat df=new DecimalFormat("0.00000");
        return Double.parseDouble(df.format(this.potenciaSaida).replace(",","."));
    }

    /**
     * Calcula o coeficiente de atenuacao
     *
     * @return coeficiente de atenuacao
     */
    private double calcularCoeficienteAtenuacao() {
        DecimalFormat df=new DecimalFormat("0.000");
        double value=1.7f * (Math.pow((0.85f / this.comprimentoOnda), 4));
        return Double.parseDouble(df.format(value).replace(",","."));
    }

    /**
     * Calcula o angulo critico da fibra otica
     *
     * @return o angulo critico
     */
    public double calcularAnguloCritico() {
        double valorRadianos = Math.asin(fo.getBainha().getIndiceMaterial() / fo.getNucleo().getIndiceMaterial());
        this.anguloCritico = Math.toDegrees(valorRadianos);
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(df.format(this.anguloCritico).replace(",","."));
    }

    /**
     * Calcula a frequência normalizada da fibra ótica em análise.
     *
     * @return frequência normalizada
     */
    public double calcularFrequenciaNormalizada() {
        double raioNucleo = this.diametroNucleo / 2;
        double valor = (2 * Math.PI * raioNucleo) / (double)this.comprimentoOnda;
        this.freqNormalizada = valor * this.aberturaNumerica;
        DecimalFormat df=new DecimalFormat("0.000");
        return Double.parseDouble(String.format("%.2e",freqNormalizada).replace(",","."));
    }

    /**
     * Classifica a fibra consoante o seu valor da frequência normalizada.
     *
     * @return classificação da fibra ótica, sendo esta classificada entre
     * monomodo ou multimodo
     */
    public String classificaFibra() {
        this.classificaFibra = "";
        if (this.freqNormalizada < 2.405) {
            this.classificaFibra += "Monomodo";
        } else {
            this.classificaFibra += "Multimodo";

        }
        return this.classificaFibra;
    }
}
