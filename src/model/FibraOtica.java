/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author G01
 */
public class FibraOtica implements Serializable{

    /**
     * Núcleo de uma fibra ótica
     */
    private Nucleo nucleo;
    /**
     * Bainha de uma fibra ótica
     */
    private Bainha bainha;

    /**
     * Constrói uma instância de FibraOtica que recebe o parâmetro núcleo e
     * bainha
     *
     * @param materialNucleo material do nucleo
     * @param indiceNucleo indice de refracao do nucleo
     * @param materialBainha material da bainha
     * @param bainha a bainha de uma fibra ótica
     */
    public FibraOtica(String materialNucleo, float indiceNucleo,
            String materialBainha, float indiceBainha) {
        this.nucleo = new Nucleo(materialNucleo, indiceNucleo);
        this.bainha = new Bainha(materialBainha, indiceBainha);

    }

    /**
     * Constroi uma instancia de fibra otica recebendo como parametros o nucleo
     * e a bainha
     *
     * @param nucleo nucleo da fibra otica
     * @param bainha bainha da fibra otica
     */
    public FibraOtica(Nucleo nucleo, Bainha bainha) {
        this.nucleo = nucleo;
        this.bainha = bainha;
    }

    /**
     * Constrói uma instância de FibraOtica que não recebe parâmetros
     */
    public FibraOtica() {
        this.nucleo = new Nucleo();
        this.bainha = new Bainha();

    }

    /**
     * Constrói uma instância de FibraOtica recebendo como parâmetro outra
     * instância de FibraOtica
     *
     * @param fibra_copia instância de FibraOtica
     */
    public FibraOtica(FibraOtica fibra_copia) {
        this(fibra_copia.nucleo, fibra_copia.bainha);
    }

    /**
     * Devolve o Nucleo da instância FibraOtica
     *
     * @return instância Nucleo
     */
    public Nucleo getNucleo() {
        return this.nucleo;
    }

    /**
     * Devolve a Bainha da instância FibraOtica
     *
     * @return instância Bainha
     */
    public Bainha getBainha() {
        return this.bainha;
    }

    public void setNucleo(Nucleo nucleo) {
        this.nucleo = nucleo;
    }

    /**
     * Modifica a Bainha da instância FibraOtica
     *
     * @param bainha a instância bainha
     */
    public void setBainha(Bainha bainha) {
        if (this.nucleo.getIndiceMaterial() > bainha.getIndiceMaterial()) {
            this.bainha = bainha;
        } else {
            throw new IllegalArgumentException("Dados inválidos.");
        }
    }

    /**
     * Método que irá verificar se os indices de refração do nucleo e da bainha
     * estão corretos
     */
    private boolean valida() {
        if (bainha.getIndiceMaterial() >= 1 && nucleo.getIndiceMaterial() >= 1) {
            return true;
        }
        return false;
    }

    /**
     * Método que compara duas instância de FibraOtica´s
     *
     * @param obj FibraOtica a comparar
     * @return valor booleano com o resultado da comparação
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        FibraOtica outraFibraOtica = (FibraOtica) obj;
        return (this.nucleo.equals(outraFibraOtica.nucleo) && (this.bainha.equals(outraFibraOtica.bainha)));
    }

    /**
     * Método que representa textualmente a instância FibraOtica
     *
     * @return representação textual
     */
    public String toString() {
        return "Indice de refração do " + getNucleo().getNomeMaterial() + "(nucleo): " + getNucleo().getIndiceMaterial() + "\n"
                + "Indice de refração do " + getBainha().getNomeMaterial() + "(bainha): " + getBainha().getIndiceMaterial();
    }
}
