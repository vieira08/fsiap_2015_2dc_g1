/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author G01
 */
public class Material implements Serializable {

    /**
     * Lista de Materiais.
     */
    private HashMap<String, Float> listaMaterial;

    /**
     * Constrói instância de Material
     */
    public Material() {
        this.listaMaterial = new HashMap<>();
    }

    /**
     * Devolve a lista de materiais passíveis de ser utilizados no núcleo.
     *
     * @return lista de materiais do núcleo.
     */
    public Set<String> getListaMateriaisNucleo() {
        return listaMaterial.keySet();
    }

    /**
     * Devolve o nome do material associado ao indice que recebe por parâmetro
     *
     * @param indice indice do material
     * @return nome do material
     */
    public String getMaterial(float indice) {
        for (String str : listaMaterial.keySet()) {
            if (listaMaterial.get(str) == indice) {
                return str;
            }
        }
        return null;
    }

    /**
     * Devolve a lista de materiais passíveis de ser utilizados na bainha.
     *
     * @return lista de materiais da bainha.
     */
    public ArrayList<String> getListaMateriaisBainha(float indice) {
        ArrayList listaMateriaisBainha = new ArrayList<>();
        float indiceMinimoBainha = indice - (indice * 0.02f);
        for (Float ind : this.listaMaterial.values()) {
            if (indice > ind && ind >= indiceMinimoBainha) {
                listaMateriaisBainha.add(getMaterial(ind));
            }
        }
        return listaMateriaisBainha;
    }

    /**
     * Devolve o índice do material associado ao nome recebido por parâmetro
     *
     * @param material nome do material
     * @return indice do material
     */
    public float getIndice(String material) throws NullPointerException {
        return this.listaMaterial.get(material);
    }

    /**
     * Adiciona um novo material à lista de materiais disponibilizados.
     *
     * @return true caso seja adicionado e falso caso não seja.
     */
    public boolean adicionaMaterial(String nomeMaterial, float indiceRefracaoMaterial) {
        if (!listaMaterial.keySet().contains(nomeMaterial)) {
            if (!(nomeMaterial.isEmpty())) {
                if (indiceRefracaoMaterial > 1.0f) {
                    this.listaMaterial.put(nomeMaterial, indiceRefracaoMaterial);
                    return true;
                } else {
                    throw new IllegalArgumentException("Indice Invalido");
                }
            } else {
                throw new IllegalArgumentException("Nome Invalido");
            }
        }
        return false;
    }

    /**
     * Remove um material da lista de materiais
     *
     * @param material material a remover
     * @return verdadeiro se remover e falso caso não seja possível remover
     */
    public boolean removerMaterial(String material) {
        if (material!=null && this.listaMaterial.containsKey(material)) {
            this.listaMaterial.remove(material);
            return true;
        }
        return false;
    }

    /**
     * Preenche a lista de materiais do HashMap.
     */
    public void preencheListaMateriais() {
        
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("vidroBaixoIndice"), 1.5f);
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("vidroAltoIndice"), 1.9f);       
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("silexVidro"), 1.62f);       
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("vidroOtico"), 1.54f);       
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("glicerina"), 1.47f);       
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("policarbonato"), 1.59f);
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("silica"), 1.458f);
        this.listaMaterial.put(java.util.ResourceBundle.getBundle("ui/Bundle").getString("acrilico"), 1.49f);
    }

}
