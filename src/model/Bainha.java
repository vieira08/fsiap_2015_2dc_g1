package model;

import java.io.Serializable;

/**
 * Classe que representa a bainha de uma fibra ótica.
 * @author G01
 */
public class Bainha implements Serializable{
    
    /**
     * Nome do material da bainha.
     */
    private String nomeMaterial;
    
    /**
     * Índice do material da bainha.
     */
    private float indiceMaterial;
    
    /**
     * Construtor que cria uma instância de bainha com o nome e o índice recebidos como parâmetro.
     * @param nomeMaterial nome do material
     * @param indiceMaterial indice do material
     */
    public Bainha(String nomeMaterial, float indiceMaterial){
        this.nomeMaterial = nomeMaterial;
        this.indiceMaterial = indiceMaterial;
    }
    
    /**
     * Construtor vazio.
     */
    public Bainha(){
        this.nomeMaterial = null;
        this.indiceMaterial = 0f;
    }
    
    /**
     * Construtor que cria uma instânca de bainha através de outro núcleo recebido como parâmetro.
     * @param outraBainha 
     */
    public Bainha(Bainha outraBainha){
       this.nomeMaterial = outraBainha.getNomeMaterial();
       this.indiceMaterial = outraBainha.getIndiceMaterial();
    }

    /**
     * Devolve o nome do material da bainha.
     * @return nome do material
     */
    public String getNomeMaterial() {
        return nomeMaterial;
    }

    /**
     * Modifica o nome do material da bainha.
     * @param nomeMaterial nome do material
     */
    public void setNomeMaterial(String nomeMaterial) {
        this.nomeMaterial = nomeMaterial;
    }

    /**
     * Devolve o indice do material da bainha.
     * @return indice do material
     */
    public float getIndiceMaterial() {
        return indiceMaterial;
    }

    /**
     * Modifica o indice do material da bainha.
     * @param indiceMaterial indice do material
     */
    public void setIndiceMaterial(float indiceMaterial) {
        this.indiceMaterial = indiceMaterial;
    }

    /**
     * Devolve uma representação textual do nome e do índice da bainha.
     * @return 
     */
    @Override
    public String toString() {
        return "Nome da bainha: " + this.nomeMaterial + "\n Índice da bainha: " + this.indiceMaterial;
    }
    
    /**
     * Compara duas bainhas.
     * @param obj outra bainha
     * @return true se forem iguais; false se forem diferentes
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Bainha outraBainha = (Bainha) obj;
        return (this.nomeMaterial.equalsIgnoreCase(outraBainha.nomeMaterial) && this.indiceMaterial == outraBainha.indiceMaterial);
    }
}
